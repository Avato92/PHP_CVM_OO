Proyecto de PHP, javascript, HTML, CSS para la segunda evaluación de 
programación del módulo superior de desarrollo de aplicaciones web por 
Alejandro Vañó Tomás

Hecho:
	-Homepage, donde saca imagenes de base de datos
	-Carousel
	-Datatable
	-Mapa con la API de google
	-CRUD
    -PK no repetidas
    -Modal
	-Dependent dropdown y autocomplete
	-Login
	-Multilanguage
    -API del tiempo
    -Carrito de la compra
    -Like y profile con la lista de objetos a los que les ha dado like
    
Mejoras:
    -Top page para cada modulo independiente
    -Función DAO del carrito para generar factura, con sus líneas con cada producto
    -Control de errores al generar la factura
    -Dependiendo del error, eliminar el carrito del localstorage o no
    -Sistema de intentos en caso de error desconocido en el carrito de la compra, al fallar 3 veces seguidas, borra el carrito
    -Si el usuario que esta logueado no es un admin, no puede acceder al CRUD, ni aunque conozca la URL
    -Si el usuario hace checkout al carrito de la compra, si no esta logueado lo redirige al login
    -Recover password, genera un codigo aleatorio, que en teoria se le enviaria al usuario por email y deberia introducirlo para poder modificar su contraseña, como no tenemos el servicio de email introducido, lo saca con un alert para poder comprobar su funcionalidad

Pendiente:
    -CRUD de organizar eventos
    -Perfil y update

Falta perfeccionar:
	-Validación PHP, CRUD
	-Expresiones regulares en validación JS en el login
