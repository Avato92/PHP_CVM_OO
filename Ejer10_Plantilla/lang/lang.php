<?php

	if (isset($_SESSION["lang"])){
		$lang = $_SESSION["lang"];

	switch($lang){
		case 'en';
			include 'lang/en.php';
			break;
		case 'es';
			include 'lang/es.php';
			break;
		case 'val';
			include 'lang/val.php';
			break;
		default;
			include 'lang/es.php';
			break;
	}
	}else{
		include 'lang/es.php';
}

