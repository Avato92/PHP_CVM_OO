<?php

$menu = array(
	"return" => "Volver",
	"contact" => "Número de contacto",
	"mail" => "Correo electrónico",
	"direction" => "Dirección",
	"index" => "Inicio",
	"weapon" => "Armas",
	"servicis" => "Servicios",
	"about_us" => "Conocenos",
	"contact" => "Contacto",
	"submit" => "Enviar",
	"shop" => "Tiendas",
);

$controller = array(
	"registry" => "Registrado en la base de datos correctamente",
	"update" => "Actualizado en la base de datos correctamente",
	"delete" => "Borrado en la base de datos correctamente",
);

$validate_php = array(
	"fail_name" => "El nombre del arma no es correcto",
	"fail_checkbox" => "Tienes que seleccionar mínimo un complemento",
);

$validate_js = array(
	"no_name" => "Introduzca el nombre del arma",
	"no_date" => "Introduzca la fecha",
	"no_description" => "Inroduzca una descripción",
);

$create_form =  array(
	"weapon_name" => "Nombre del arma:",
	"name_placeholder" => "Escriba el nombre del arma",
	"caliber" => "Calibre:",
	"another" => "Otro",
	"country" => "País de origen:",
	"date" => "Fecha de creación:",
	"date_placeholder" => "Elija la fecha",
	"complements" => "Complementos:",
	"telescopic_sight" => "Mira telescopica",
	"cartridge_belts" => "Cananas y cartucheras",
	"bipod" => "Bípode",
	"tripod" => "Trípode",
	"anothers" => "Otros",
	"description" => "Descripción del arma:",
	"description_placeholder" => "Escriba aquí una pequeña descripción del arma",
	"bottom" => "Guardar",
);

$delete = array(
	/*"sure" => "¿Desea borrar el arma <?php echo $_GET['id'];?>",
	*/"yes_bottom" => "Aceptar",
	"no_bottom" => "Cancelar",
);

$read = array(
	"information" => "Información del arma:",
	"weapon" => "Arma: ",
	"caliber" => "Calibre: ",
	"country" => "País: ",
	"date" => "Fecha de creación: ",
	"complements" => "Complementos: ",
	"description" => "Descripción: ",
	"return" => "Volver",
);

$error = array(
	"return" => "Volver",
);

$top_page = array(
	"title" => "Armas",
);

$top_page_weapon = array(
	"title" => "Alta de arma",
);