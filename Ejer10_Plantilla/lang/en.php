<?php

$menu = array(
	"return" => "return",
	"contact" => "Contact number",
	"mail" => "Email",
	"direction" => "Direction",
	"index" => "Home",
	"weapon" => "Weapons",
	"servicis" => "Servicis",
	"about_us" => "About us",
	"contact" => "Contact",
	"submit" => "Submit",
	"shop" => "Shops",
);

$controller = array(
	"registry" => "Registered in the database correctly",
	"update" => "Updated in the database correctly",
	"delete" => "Deleted in the database correctly",
);

$validate_php = array(
	"fail_name" => "The name of the weapon is not correct",
	"fail_checkbox" => "You have to select at least one add-on",
);

$validate_js = array(
	"no_name" => "Insert the name of the weapon",
	"no_date" => "Insert the date",
	"no_description" => "Insert a description",
);

$create_form =  array(
	"weapon_name" => "Weapon name:",
	"name_placeholder" => "Insert the weapon name",
	"caliber" => "Caliber:",
	"another" => "Another",
	"country" => "Country of origin:",
	"date" => "Creation date:",
	"date_placeholder" => "Choose the date",
	"complements" => "Complements:",
	"telescopic_sight" => "Telescopic Sight",
	"cartridge_belts" => "Cartridge belts",
	"bipod" => "Bipod",
	"tripod" => "Tripod",
	"anothers" => "Anothers",
	"description" => "Description of the weapon:",
	"description_placeholder" => "Insert a description of the weapon",
	"bottom" => "Save",
);

$delete = array(
	/*"sure" => "Do you want to delete the weapon <?php echo $_GET['id'];?>",
	*/"yes_bottom" => "Yes",
	"no_bottom" => "Cancel",
);

$read = array(
	"information" => "Information about the weapon:",
	"weapon" => "Weapon: ",
	"caliber" => "Caliber: ",
	"country" => "Country: ",
	"date" => "Creation date: ",
	"complements" => "Complements: ",
	"description" => "Description: ",
	"return" => "Return",
);

$error = array(
	"return" => "Return",
);

$top_page = array(
	"title" => "Weapons",
);

$top_page_weapon = array(
	"title" => "Register a weapon",
);