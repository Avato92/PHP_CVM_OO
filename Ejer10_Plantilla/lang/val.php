<?php

$menu = array(
	"return" => "Tornar",
	"contact" => "Número de contacte",
	"mail" => "Correu electrónic",
	"direction" => "Direcció",
	"index" => "Inici",
	"weapon" => "Armes",
	"servicis" => "Servicis",
	"about_us" => "Coneix-nos",
	"contact" => "Contacte",
	"submit" => "Enviar",
	"shop" => "Tendes",
);

$controller = array(
	"registry" => "Registrat en la base de dades correctament",
	"update" => "Actualitzat en la base de dades correctament",
	"delete" => "Borrat en la base de dades correctament",
);

$validate_php = array(
	"fail_name" => "El nom del arma no es correte",
	"fail_checkbox" => "Tens que seleccionar mínim un complement",
);

$validate_js = array(
	"no_name" => "Introduïu el nom de l'arma",
	"no_date" => "Introduïu la data",
	"no_description" => "Introduïu una descripció",
);

$create_form = array(
	"weapon_name" => "Nom del arma:",
	"name_placeholder" => "Escriu el nom del arma",
	"caliber" => "Calibre:",
	"another" => "Altre",
	"country" => "País d' origen:",
	"date" => "Data de creació:",
	"date_placeholder" => "Triï la data",
	"complements" => "Complements:",
	"telescopic_sight" => "Mira telescopica",
	"cartridge_belts" => "Cartutxeres",
	"bipod" => "Bípode",
	"tripod" => "Trípode",
	"anothers" => "Altres",
	"description" => "Descripció del arma:",
	"description_placeholder" => "Escriu una descripció del arma",
	"bottom" => "Guardar",
);

$delete = array(
	/*"sure" => "Desitja borrar el arma <?php echo $_GET['id'];?>",
	*/"yes_bottom" => "Aceptar",
	"no_bottom" => "Cancelar",
);

$read = array(
	"information" => "Informació del arma:",
	"weapon" => "Arma: ",
	"caliber" => "Calibre: ",
	"country" => "País: ",
	"date" => "Data de creació: ",
	"complements" => "Complements: ",
	"description" => "Descripció: ",
	"return" => "Tornar",
);

$error = array(
	"return" => "Tornar",
);

$top_page = array(
	"title" => "Armes",
);

$top_page_weapon = array(
	"title" => "Alta d' arma",
);