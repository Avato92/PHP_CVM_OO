<?php
    $path = $_SERVER['DOCUMENT_ROOT'] . '/Ejer10_Plantilla/';
    include ($path . "module/shop/model/DAOShop.php");
	
    if(!isset($_SESSION)){
    session_start();
    }

    switch($_GET['op']){

    	case 'list';
    		try{
    			$daocity = new DAOShop();
    			$rdo = $daocity->select_all_cities();
    		}catch (Exception $e){
    			$callback = 'index.php?page=503';
			    die('<script>window.location.href="'.$callback .'";</script>');
    		}
    		if(!$rdo){
    			$callback = 'index.php?page=503';
			    die('<script>window.location.href="'.$callback .'";</script>');
    		}else{
     		include("module/shop/view/shop.php");
     	}
     		break;

        case 'find';
            try{
                $daoshop = new DAOShop();
			    $rdo = $daoshop->select_shop($_GET['cityID']);
            }catch (Exception $e){
                echo json_encode("error");
                exit;
            }
            if(!$rdo){
                echo json_encode("error");
                exit;
            }else{
                //print_r($rdo);
                echo json_encode($rdo);
                exit;
            }           
            break;

        case 'autocomplete';
            try{
                $daoshop = new DAOShop();
                // print_r($_GET['key'],$_GET['shop']);
                // die();
                $rdo = $daoshop->select_items($_GET['key'],$_GET['shop']);
                // print_r($rdo);
                // die();
            }catch (Exception $e){
                echo json_encode("error");
                exit;
            }
            if(!$rdo){
                echo json_encode("error");
                exit;
            }else{
                 // print_r($rdo);
                 // die();
                echo json_encode($rdo);
                exit;
            }
            break;
        case 'read';
                 // print_r("Hola");
                 // die(); 
            try{
                $daoitem = new DAOShop();
                // print_r($_GET['item']);
                // die();
                $rdo = $daoitem ->select_item($_GET['item']);
                // print_r($rdo);
                // die();
            }catch (Exception $e){
                echo json_encode("error");
                exit;
            }if(!$rdo){
                echo json_encode("error");
                exit;
            }else{
                $item=get_object_vars($rdo);
                echo json_encode($item);
                exit;
            }
            break;
        case 'cart';
            include("module/shop/view/cart.php");
            break;
        case 'checkout';
            if (isset($_SESSION['user'])){
                $user = $_SESSION['user'];
                $data = json_decode(stripcslashes($_POST['data']));
                // echo json_encode($data);
                // exit();
                     try{
                        $daocheckout = new DAOShop();
                        $rdo = $daocheckout ->checkout_invoice($user, $data);
                         // echo json_encode($rdo);
                         // exit();
                    }catch (Exception $e){
                        echo json_encode("error");
                        exit;
                    }  
                    echo json_encode($rdo);
                    exit;  
            }else{
                include("module/login/view/sign.php");
            }
        case 'like';
            $user = $_SESSION['user'];
            $item = $_POST['data'];
            // echo json_encode($item);
            // exit();
            try{
                $daolike = new DAOShop();
                $rdo = $daolike -> like_item($user, $item);
            }catch(Exception $e){
                echo json_encode("error");
                exit;
            }
            echo json_encode($rdo);
            exit();
            break;
        default;
            include("view/inc/error404.php");
            break;
	}


    