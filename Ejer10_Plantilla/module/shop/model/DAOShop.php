<?php

    $path = $_SERVER['DOCUMENT_ROOT'] . '/Ejer10_Plantilla/';
    include($path . "model/connect.php");

    	class DAOShop{
    		function select_shop($cityID){
    			$sql = "SELECT * FROM shop WHERE cityID='$cityID'";
			 //print_r ($sql);
			    $conexion = connect::con();
                $res = mysqli_query($conexion, $sql);
                connect::close($conexion);
                foreach ($res as $row) {
                    $form[]=array_merge($row);
                }
                return $form;
		}
            function select_all_cities(){
                $sql = "SELECT * FROM city";
                
                $conexion = connect::con();
                $res = mysqli_query($conexion, $sql);
                connect::close($conexion);
                return $res;
        }
            function select_items($key, $shop){
                $sql="SELECT * FROM items WHERE item_name LIKE '%" . $key . "%' AND shopID = '$shop'";
                 //print_r($sql);
                 //die();

                $conexion = connect::con();
                $res = mysqli_query($conexion, $sql);
                connect::close($conexion);
                foreach($res as $row) {
                    $form[]=array_merge($row);
                }
                return $form;
            }
            function select_item($item){
                $sql="SELECT DISTINCT * FROM items WHERE item_name='$item'";
                $conexion = connect::con();
                $res = mysqli_query($conexion, $sql)->fetch_object();
                connect::close($conexion);
                return $res;
            }
            function checkout_invoice($user, $data){
                $sql = "INSERT INTO sales_check (user, sale_date) VALUES ('$user', now())";
                $conexion = connect::con();
                $res = mysqli_query($conexion, $sql);
                $number_line = 0;
                $amount = 0;
                if ($res == true){
                    $search_id = "SELECT MAX(id) FROM sales_check";
                    $rs = mysqli_query($conexion, $search_id);
                    if ($row = mysqli_fetch_row($rs)) {
                        $id = trim($row[0]);
                        //return $id;
                    }foreach($data as $clave){
                        $item = $clave;
                    //return $item->Producto;            
                        $product = $item->Producto;
                        //$conexion = connect::con();
                        $sql_product = "SELECT DISTINCT price FROM items WHERE item_name = '$product'";
                        $rdo_price = mysqli_query($conexion, $sql_product);
                        if ($rdo_price == false){
                            $sql_delete ="DELETE FROM sales_check WHERE id = '$id'";
                            $rdo_delete = mysqli_query($conexion, $sql_delete);
                            connect::close($conexion);
                            $error = 6;
                            return $error;
                        }
                        if ($row = mysqli_fetch_row($rdo_price)){
                            $price = trim($row[0]);
                            //return $price;
                        }
                        ++$number_line;
                        $quantity = $item->quantity;
                        if ($quantity <= 0){
                            $sql_delete ="DELETE FROM sales_check WHERE id = '$id'";
                            $rdo_delete = mysqli_query($conexion, $sql_delete);
                            connect::close($conexion);
                            $error = 5;
                            return $error;
                        }
                        //return $quantity;
                        //$price = $item->Precio;
                        //return $price;
                        //$total = $item->Amount;
                        $total = $price * $quantity;
                        //return $total;
                        if ($total <= 0){
                            $sql_delete = "DELETE FROM sales_check WHERE id = '$id'";
                            $rdo_delete = mysqli_query($conexion, $sql_delete);
                            connect::close($conexion);
                            $error = 4;
                            return $error;
                        }
                        $amount = $amount + $total;
                        //return $total;
                        $sql_line = "INSERT INTO invoice_line (id_line, id_invoice, product, quantity, price, total) VALUES ('$number_line', '$id', '$product', '$quantity', '$price', '$total')";
                        $finish = mysqli_query($conexion, $sql_line);
                        //return $sql_line;
                }
                //return $finish;
                if ($finish == true){
                    $sql_update = "UPDATE sales_check SET amount = '$amount' WHERE id = '$id'";
                    $finish_update = mysqli_query($conexion, $sql_update);
                //return $finish_update;
                if ($finish_update == true){
                    connect::close($conexion);
                    $error = 0;
                    return $error;
                }else{
                $sql_delete = "DELETE FROM sales_check WHERE id = '$id'";
                $sql_delete_line = "DELETE FROM invoice_line WHERE id_invoice = '$id'";
                $rdo_delete = mysqli_query($conexion, $sql_delete);
                $rdo_delete_line = mysqli_query($conexion, $sql_delete_line);
                connect::close($conexion);
                    $error = 3;
                    return $error;
                }
            }else{
                $sql_delete = "DELETE FROM sales_check WHERE id = '$id'";
                $sql_delete_line = "DELETE FROM invoice_line WHERE id_invoice = '$id'";
                $rdo_delete = mysqli_query($conexion, $sql_delete);
                $rdo_delete_line = mysqli_query($conexion, $sql_delete_line);
                connect::close($conexion);
                $error = 2;
                return $error;
            }
        }else{
                connect::close($conexion);
                $error = 1;
                return $error;
        }
    }
    function like_item($user, $item){
        $conexion = connect::con();
        $sql_like = "INSERT INTO favorites (user, like_date, item) VALUES ('$user', now(), '$item')";
        $rdo = mysqli_query($conexion, $sql_like);
        if ($rdo == true){
            $error = 0;
        }else{
            $error = 1;
        }
        connect::close($conexion);
        return $error;
    }
}