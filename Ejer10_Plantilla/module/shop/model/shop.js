

function getShop(val) {
	// console.log(val);
	$.get("module/shop/controller/controller_shop.php?op=find&cityID="+ val, function (data, status) {
		 //console.log(data);
		var datos = JSON.parse(data);

	    $("#shop-list").empty();
	    $("#shop-list").append('<option value="" selected="selected">Tiendas</option>');
	    $.each(datos, function (i, valor) {
	        $("#shop-list").append('<option value="' + valor.id + '">' + valor.name + '</option>');
		});
	});
	}
$(document).ready(function(){
	//console.log("Funciona");
	$("#tags").keyup(function(){
		//alert("Entramos");
		var key = $('#tags').val();
		var shop = $('#shop-list').val();
		//alert (shop);
		//alert(keyup);
		$.get("module/shop/controller/controller_shop.php?op=autocomplete&key=" + key + "&shop=" + shop,


		 function(data, status){
			//console.log(data);
			var datos = JSON.parse(data);
			//console.log(datos);
			var dato  = [];
			var id = [];

				//console.log(datos);

			if(datos === 'error'){
				window.location.href='index.php?page=503';
			}
			else{
				datos.forEach(function(element){
					 dato.push(element['item_name']);
					 id.push(element['codigo']);
					 // console.log(dato);
					
				});
				 //console.log(id);

				 $('#tags').autocomplete({
				 	source:  dato,
				 	select: function(event, ui){
				 		//console.log(ui.item.value);
				 		$.get("module/shop/controller/controller_shop.php?op=read&item="+ ui.item.value, function(data, status){
				 			// console.log(data);
				 			// console.log(data.price);
				 			var json = JSON.parse(data);
				 			//console.log(json);
    
                
                			if(json === 'error') {
    			    			window.location.href='index.php?page=503';
                			}else{
                				$("#item_name").html(json.item_name);
			                    $("#type").html(json.type);
			                    $("#country").html(json.country);
			                    $("#description").html(json.description);
			                    $('#price_item').html(json.price);
			                    $('#error_quantity').html("");
			         
			                    $("#details_item").show();
			                    $("#item_modal").dialog({
			                        width: 850, //<!-- ------------- ancho de la ventana -->
			                        height: 500, //<!--  ------------- altura de la ventana -->
			                        show: "scale", //<!-- ----------- animación de la ventana al aparecer -->
			                        hide: "scale", //<!-- ----------- animación al cerrar la ventana -->
			                        resizable: "false", //<!-- ------ fija o redimensionable si ponemos este valor a "true" -->
			                        position: "center",//<!--  ------ posicion de la ventana en la pantalla (left, top, right...) -->
			                        modal: "true", //<!-- ------------ si esta en true bloquea el contenido de la web mientras la ventana esta activa (muy elegante) -->
			                        buttons: {
			                            Ok: function () {
			                                $(this).dialog("close");
			                            }
			                        },
			                        show: {
			                            effect: "blind",
			                            duration: 1000
			                        },
			                        hide: {
			                            effect: "blind",
			                            duration: 1000
			                        }
			                    });

                			}
				 				});
				 			}
				 		});
				 	}
				});
			});
		});