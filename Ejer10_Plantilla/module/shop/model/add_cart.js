var cart = [];
var attempt = 0;  
        $(function () {
        	//alert("Hola");
            if (localStorage.cart){
                cart = JSON.parse(localStorage.cart);  
                showCart();
            }
        });

function saveCart() {
            if (window.localStorage)
            {
                localStorage.cart = JSON.stringify(cart);
            }
        }

function showCart() {
      var element = document.getElementById("checkout");
            if (cart.length == 0) {
                $("#cart").css("visibility", "hidden");
                //element.style.display = 'none';
                $("#checkout").css("visibility", "hidden");
                return;
            }

            $("#cart").css("visibility", "visible");
            $("#checkout").css("visibility", "visible");
            //element.style.display = 'block';
            $("#cartBody").empty();
            for (var i in cart) {
                var items = cart[i];
                var row = "<tr><td>" + items.Producto + "</td><td>" +
                       items.Precio + "</td><td>" + items.quantity + "</td><td>"
                       + items.quantity * items.Precio + "</td><td>"
                       + "<button onclick='deleteItem(" + i + ")'>Delete</button></td></tr>";
                $("#cartBody").append(row);
            }
        }

function deleteItem(index){
            cart.splice(index,1);
            showCart();
            saveCart();
        }
function deleteCart(){
        localStorage.clear();
        cart = [];
        saveCart();
        showCart();
}


$(document).ready(function(){
 
	//alert("Hola soy add_cart.js");
	$('.add_cart').click(function () {
		//alert("Hola funciono");
		var item = document.getElementById("item_name").innerHTML;
		var num = document.getElementById('n_items').value;
        if (num <= 0){
            document.getElementById('error_quantity').innerHTML = "La cantidad no puede ser negativa";
            return 0; 
        }
		var price = document.getElementById('price_item').innerHTML;
        var total = (parseInt(num) * parseInt(price));
        //console.log(total);
		// console.log(item);
		// console.log(num);
		//console.log(price);
		var items = {Producto: item, Precio: price, quantity: num, Amount: total};
        //console.log(items);
		cart.push(items);
		saveCart();
		showCart();
	});
    $('.to_cart').click(function () {
        //alert("Funciono");
        location.href="index.php?page=controller_shop&op=cart";
    });
    $("#like").click(function(){
        //alert("Funciono");
        var item = document.getElementById('item_name').innerHTML;
        //alert(item);
         $.ajax({
             type: "POST",
             url: "module/shop/controller/controller_shop.php?op=like",
             data: {data : item},
            success: function(data, status){
                //alert(data);
                if (data == 0){
                    document.getElementById('error_quantity').innerHTML="Producto añadido a favoritos";
                }else if(data == 1){
                    document.getElementById('error_quantity').innerHTML="Ya lo tienes en favoritos";
                }else{
                    alert("Error en la conexión");
                }
            }
        });
    });
    $('.checkout').click(function(){

        //console.log(cart);
        //alert("Funciono");
        var jsonString = JSON.stringify(cart);
        $.ajax({
             type: "POST",
             url: "module/shop/controller/controller_shop.php?op=checkout",
             data: {data : jsonString},
            success: function(data, status){
                console.log(data);

                if(data == 0){
                    alert("Compra realizada con éxito");
                    deleteCart();
                }else if(data == 1){
                    alert("Error al generar la factura");
                }else if(data == 2){
                    alert("Error al generar las lineas");
                }else if (data == 3){
                    alert("Error al actualizar la factura");
                }else if(data == 4){
                    alert("Error en el precio");
                    deleteCart();
                }else if(data == 5){
                    alert("Error en la cantidad");
                    deleteCart();
                }else if (data == 6){
                    alert("Error, objeto no encontrado");
                    deleteCart();
                }else{
                    attempt ++;
                    alert("Ha fallado algo");
                    if (attempt == 3){
                        deleteCart();
                        attempt = 0;
                    }
                }
            }
         });
    });
});