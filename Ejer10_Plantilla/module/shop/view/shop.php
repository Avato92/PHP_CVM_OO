<section class="center">

<h1>Encuentra tu tiendas más cercana:</h1>

<div class="frmDronpDown">
	<div class="row">
		<label>Ciudad:</label><br/>
			<select name="city" id="city-list" class="demoInputBox" onChange="getShop(this.value);">
				<option value=""></option>
				<?php
				foreach($rdo as $city) {
				?>
				<option value="<?php echo $city["id"]; ?>"><?php echo $city["city_name"]; ?></option>
				<?php
				}
				?>
			</select>
	</div>
	<div class="row">
		<label>Tiendas:</label><br/>
			<select name="shop" id="shop-list" class="demoInputBox">
				<option value=""></option>
			</select>
			<br>
			<section>
				<div class="ui-widget">
	  				<label for="tags">Busca el producto que desees: </label>
	  				<input id="tags" class="tags"/>
				</div>
				<div>
					<button type="button" name="to_cart" id="to_cart" class="to_cart">Cesta</button>
			</section>
	</div>
</div>

<section id="item_modal">
    <section>
    <div id="details_item" hidden>
        <div id="details">
            <div id="container">
                        Nombre del producto: <div id="item_name"></div></br>
                        Tipo: <div id="type"></div></br>
                        País de origen: <div id="country"></div></br>
                        Descripción: <div id="description"></div></br>
                        Precio: <label id="price_item"></label><label> Euros</label></br>
                        <input type="number" id="n_items" class="n_items" min="1"/>
                        <button type="button" name="add_cart" id="add_cart" class="add_cart"><img src="view/img/carrito.png" height="20" width="20" /></button>
                        <button type="button" name="like" id="like" class="like">Me gusta</button>
                        <span id="error_quantity"></span>
            </div>
        </div>
    </div>
</section>
</section>

<!-- <table id="cart" border="1" style="visibility:hidden; width:100%">
         <thead>
              <tr>
                  <th>Producto</th>
                  <th>Precio</th>
                  <th>Cantidad</th>
                  <th>Total</th>
                  <th></th>
             </tr>
         </thead>
         <tbody id="cartBody">

         </tbody>
    </table> -->


