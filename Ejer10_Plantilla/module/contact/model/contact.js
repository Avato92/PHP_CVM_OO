      function initMap() {
        var ontinyent = {lat: 38.8191359, lng: -0.6073138};
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 10,
          center: ontinyent
        });

        var infowindow = new google.maps.InfoWindow({
          content: contentString
        });

        var marker = new google.maps.Marker({
          position: ontinyent,
          map: map,
          title: 'Ontinyent (Vall de Albaida)'
        });
        marker.addListener('click', function() {
          infowindow.open(map, marker);
        });
      }