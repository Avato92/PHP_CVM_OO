<section class="center">
	<h1>Consulta tu tiempo</h1>
	<section class="center">
		<div>
			<div style="width: 500px; height: 400px;">
				<h4>Elige la ciudad que quieres consultar el tiempo:</h4>
				<div>
					<button type="button" class="button_weather" id="ont_button">Ontinyent</button>
					<button type="button" class="button_weather" id="boc_button">Bocairent</button>
					<button type="button" class="button_weather" id="agu_button">Agullent</button>
					<button type="button" class="button_weather" id="aie_button">Aielo</button>
					<button type="button" class="button_weather" id="xat_button">Xátiva</button>
					<button type="button" class="button_weather" id="alb_button">Albaida</button>
					<button type="button" class="button_weather" id="oll_button">Olleria</button>
					<button type="button" class="button_weather" id="fon_button">Fontanars</button>
					<button type="button" class="button_weather" id="val_button">Valencia</button>
    			</div>
    		</div>
		</div>
	</section>
</section>

<!-- modal window -->
<section id="weather_modal" class="center">
    <section>
    <div id="details_weather" hidden>
        <div id="details">
            <div id="container">
                        Tiempo: <div id="weather_api"></div></br>
                        Probabilidad de lluvia: <div id="precipProbability"></div></br>
                        Temperatura: <div id="temperature_api"></div></br>
                        Velocidad del viento: <div id="wind_velocity"></div></br>
                        Ráfagas de viento: <div id="wind_gust"></div></br>
                        Tiempo para toda la semana: <div id="weather_week"></div></br>
                        <button type="button" id="new_event" class="new_event" name="new_event">Organizar evento</button></br>
            </div>
        </div>
    </div>
</section>
</section>