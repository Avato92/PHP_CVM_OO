//alert("Hola");
//Posible mejora, resumir esto
$(document).ready(function(){
	//console.log("Hola");
	$("#ont_button").click(function(){
		//https://api.darksky.net/forecast/8f3c5abf8f60348fe538f3f5cb849182/38.8330783,-0.6243355?units=ca&excludeminutely,hourly,flags&lang=es
			 $.ajax({
            type: "GET",
            mode: 'cors',
            url: "https://api.darksky.net/forecast/8f3c5abf8f60348fe538f3f5cb849182/38.8330783,-0.6243355?units=ca&excludeminutely,hourly,flags&lang=es",
            success: function(data, status){
                //console.log(data);

                

                if(data === "error"){
                	window.location = "index.php?page=homepage&op=list";
                }else{
                	$("#weather_api").html(data.currently.summary);
                    $("#precipProbability").html(data.currently.precipProbability + "%");
                    $("#temperature_api").html(data.currently.temperature + "ºC");
                    $("#wind_velocity").html(data.currently.windSpeed);
                    $("#wind_gust").html(data.currently.windGust);
                    $("#weather_week").html(data.daily.summary);
         
                    $("#details_weather").show();
                    $("#weather_modal").dialog({
                        width: 850, //<!-- ------------- ancho de la ventana -->
                        height: 600, //<!--  ------------- altura de la ventana -->
                        show: "scale", //<!-- ----------- animación de la ventana al aparecer -->
                        hide: "scale", //<!-- ----------- animación al cerrar la ventana -->
                        resizable: "false", //<!-- ------ fija o redimensionable si ponemos este valor a "true" -->
                        position: "center",//<!--  ------ posicion de la ventana en la pantalla (left, top, right...) -->
                        modal: "true", //<!-- ------------ si esta en true bloquea el contenido de la web mientras la ventana esta activa (muy elegante) -->
                        buttons: {
                            Ok: function () {
                                $(this).dialog("close");
                            }
                        },
                        show: {
                            effect: "blind",
                            duration: 1000
                        },
                        hide: {
                            effect: "blind",
                            duration: 1000
                        }
                    });
                }
                
            }
		});
	 });
	$("#boc_button").click(function(){
		//alert("Me has pulsado");
		 $.ajax({
            type: "GET",
            mode: 'cors',
            url: "https://api.darksky.net/forecast/8f3c5abf8f60348fe538f3f5cb849182/38.7644091,-0.6171401,16?units=ca&excludeminutely,hourly,flags&lang=es",
            success: function(data, status){
                //console.log(data);

                

                if(data === "error"){
                	window.location = "index.php?page=homepage&op=list";
                }else{
                	$("#weather_api").html(data.currently.summary);
                    $("#precipProbability").html(data.currently.precipProbability + "%");
                    $("#temperature_api").html(data.currently.temperature + "ºC");
                    $("#wind_velocity").html(data.currently.windSpeed);
                    $("#wind_gust").html(data.currently.windGust);
                    $("#weather_week").html(data.daily.summary);
         
                    $("#details_weather").show();
                    $("#weather_modal").dialog({
                        width: 850, //<!-- ------------- ancho de la ventana -->
                        height: 600, //<!--  ------------- altura de la ventana -->
                        show: "scale", //<!-- ----------- animación de la ventana al aparecer -->
                        hide: "scale", //<!-- ----------- animación al cerrar la ventana -->
                        resizable: "false", //<!-- ------ fija o redimensionable si ponemos este valor a "true" -->
                        position: "center",//<!--  ------ posicion de la ventana en la pantalla (left, top, right...) -->
                        modal: "true", //<!-- ------------ si esta en true bloquea el contenido de la web mientras la ventana esta activa (muy elegante) -->
                        buttons: {
                            Ok: function () {
                                $(this).dialog("close");
                            }
                        },
                        show: {
                            effect: "blind",
                            duration: 1000
                        },
                        hide: {
                            effect: "blind",
                            duration: 1000
                        }
                    });
                }
                
            }
		});
	});
	$("#agu_button").click(function(){
		//alert("Me has pulsado");
				 $.ajax({
            type: "GET",
            mode: 'cors',
            url: "https://api.darksky.net/forecast/8f3c5abf8f60348fe538f3f5cb849182/38.7644091,-0.5595846,15?units=ca&excludeminutely,hourly,flags&lang=es",
            success: function(data, status){
                //console.log(data);

                

                if(data === "error"){
                	window.location = "index.php?page=homepage&op=list";
                }else{
                	$("#weather_api").html(data.currently.summary);
                    $("#precipProbability").html(data.currently.precipProbability + "%");
                    $("#temperature_api").html(data.currently.temperature + "ºC");
                    $("#wind_velocity").html(data.currently.windSpeed);
                    $("#wind_gust").html(data.currently.windGust);
                    $("#weather_week").html(data.daily.summary);
         
                    $("#details_weather").show();
                    $("#weather_modal").dialog({
                        width: 850, //<!-- ------------- ancho de la ventana -->
                        height: 600, //<!--  ------------- altura de la ventana -->
                        show: "scale", //<!-- ----------- animación de la ventana al aparecer -->
                        hide: "scale", //<!-- ----------- animación al cerrar la ventana -->
                        resizable: "false", //<!-- ------ fija o redimensionable si ponemos este valor a "true" -->
                        position: "center",//<!--  ------ posicion de la ventana en la pantalla (left, top, right...) -->
                        modal: "true", //<!-- ------------ si esta en true bloquea el contenido de la web mientras la ventana esta activa (muy elegante) -->
                        buttons: {
                            Ok: function () {
                                $(this).dialog("close");
                            }
                        },
                        show: {
                            effect: "blind",
                            duration: 1000
                        },
                        hide: {
                            effect: "blind",
                            duration: 1000
                        }
                    });
                }
                
            }
		});
	});
	$("#aie_button").click(function(){
		//alert("Me has pulsado");
				 $.ajax({
            type: "GET",
            mode: 'cors',
            url: "https://api.darksky.net/forecast/8f3c5abf8f60348fe538f3f5cb849182/38.8789536,-0.5941207,16?units=ca&excludeminutely,hourly,flags&lang=es",
            success: function(data, status){
                //console.log(data);

                

                if(data === "error"){
                	window.location = "index.php?page=homepage&op=list";
                }else{
                	$("#weather_api").html(data.currently.summary);
                    $("#precipProbability").html(data.currently.precipProbability + "%");
                    $("#temperature_api").html(data.currently.temperature + "ºC");
                    $("#wind_velocity").html(data.currently.windSpeed);
                    $("#wind_gust").html(data.currently.windGust);
                    $("#weather_week").html(data.daily.summary);
         
                    $("#details_weather").show();
                    $("#weather_modal").dialog({
                        width: 850, //<!-- ------------- ancho de la ventana -->
                        height: 600, //<!--  ------------- altura de la ventana -->
                        show: "scale", //<!-- ----------- animación de la ventana al aparecer -->
                        hide: "scale", //<!-- ----------- animación al cerrar la ventana -->
                        resizable: "false", //<!-- ------ fija o redimensionable si ponemos este valor a "true" -->
                        position: "center",//<!--  ------ posicion de la ventana en la pantalla (left, top, right...) -->
                        modal: "true", //<!-- ------------ si esta en true bloquea el contenido de la web mientras la ventana esta activa (muy elegante) -->
                        buttons: {
                            Ok: function () {
                                $(this).dialog("close");
                            }
                        },
                        show: {
                            effect: "blind",
                            duration: 1000
                        },
                        hide: {
                            effect: "blind",
                            duration: 1000
                        }
                    });
                }
                
            }
		});
	});
	$("#xat_button").click(function(){
		//alert("Me has pulsado");
				 $.ajax({
            type: "GET",
            mode: 'cors',
            url: "https://api.darksky.net/forecast/8f3c5abf8f60348fe538f3f5cb849182/38.9908611,-0.5305654,15?units=ca&excludeminutely,hourly,flags&lang=es",
            success: function(data, status){
                //console.log(data);

                

                if(data === "error"){
                	window.location = "index.php?page=homepage&op=list";
                }else{
                	$("#weather_api").html(data.currently.summary);
                    $("#precipProbability").html(data.currently.precipProbability + "%");
                    $("#temperature_api").html(data.currently.temperature + "ºC");
                    $("#wind_velocity").html(data.currently.windSpeed);
                    $("#wind_gust").html(data.currently.windGust);
                    $("#weather_week").html(data.daily.summary);
         
                    $("#details_weather").show();
                    $("#weather_modal").dialog({
                        width: 850, //<!-- ------------- ancho de la ventana -->
                        height: 600, //<!--  ------------- altura de la ventana -->
                        show: "scale", //<!-- ----------- animación de la ventana al aparecer -->
                        hide: "scale", //<!-- ----------- animación al cerrar la ventana -->
                        resizable: "false", //<!-- ------ fija o redimensionable si ponemos este valor a "true" -->
                        position: "center",//<!--  ------ posicion de la ventana en la pantalla (left, top, right...) -->
                        modal: "true", //<!-- ------------ si esta en true bloquea el contenido de la web mientras la ventana esta activa (muy elegante) -->
                        buttons: {
                            Ok: function () {
                                $(this).dialog("close");
                            }
                        },
                        show: {
                            effect: "blind",
                            duration: 1000
                        },
                        hide: {
                            effect: "blind",
                            duration: 1000
                        }
                    });
                }
                
            }
		});
	});
	$("#alb_button").click(function(){
		//alert("Me has pulsado");
				 $.ajax({
            type: "GET",
            mode: 'cors',
            url: "https://api.darksky.net/forecast/8f3c5abf8f60348fe538f3f5cb849182/38.8390633,-0.5231752,16?units=ca&excludeminutely,hourly,flags&lang=es",
            success: function(data, status){
                //console.log(data);

                

                if(data === "error"){
                	window.location = "index.php?page=homepage&op=list";
                }else{
                	$("#weather_api").html(data.currently.summary);
                    $("#precipProbability").html(data.currently.precipProbability + "%");
                    $("#temperature_api").html(data.currently.temperature + "ºC");
                    $("#wind_velocity").html(data.currently.windSpeed);
                    $("#wind_gust").html(data.currently.windGust);
                    $("#weather_week").html(data.daily.summary);
         
                    $("#details_weather").show();
                    $("#weather_modal").dialog({
                        width: 850, //<!-- ------------- ancho de la ventana -->
                        height: 600, //<!--  ------------- altura de la ventana -->
                        show: "scale", //<!-- ----------- animación de la ventana al aparecer -->
                        hide: "scale", //<!-- ----------- animación al cerrar la ventana -->
                        resizable: "false", //<!-- ------ fija o redimensionable si ponemos este valor a "true" -->
                        position: "center",//<!--  ------ posicion de la ventana en la pantalla (left, top, right...) -->
                        modal: "true", //<!-- ------------ si esta en true bloquea el contenido de la web mientras la ventana esta activa (muy elegante) -->
                        buttons: {
                            Ok: function () {
                                $(this).dialog("close");
                            }
                        },
                        show: {
                            effect: "blind",
                            duration: 1000
                        },
                        hide: {
                            effect: "blind",
                            duration: 1000
                        }
                    });
                }
                
            }
		});
	});
	$("#oll_button").click(function(){
		//alert("Me has pulsado");
				 $.ajax({
            type: "GET",
            mode: 'cors',
            url: "https://api.darksky.net/forecast/8f3c5abf8f60348fe538f3f5cb849182/38.9140195,-0.557973,15?units=ca&excludeminutely,hourly,flags&lang=es",
            success: function(data, status){
                //console.log(data);

                

                if(data === "error"){
                	window.location = "index.php?page=homepage&op=list";
                }else{
                	$("#weather_api").html(data.currently.summary);
                    $("#precipProbability").html(data.currently.precipProbability + "%");
                    $("#temperature_api").html(data.currently.temperature + "ºC");
                    $("#wind_velocity").html(data.currently.windSpeed);
                    $("#wind_gust").html(data.currently.windGust);
                    $("#weather_week").html(data.daily.summary);
         
                    $("#details_weather").show();
                    $("#weather_modal").dialog({
                        width: 850, //<!-- ------------- ancho de la ventana -->
                        height: 600, //<!--  ------------- altura de la ventana -->
                        show: "scale", //<!-- ----------- animación de la ventana al aparecer -->
                        hide: "scale", //<!-- ----------- animación al cerrar la ventana -->
                        resizable: "false", //<!-- ------ fija o redimensionable si ponemos este valor a "true" -->
                        position: "center",//<!--  ------ posicion de la ventana en la pantalla (left, top, right...) -->
                        modal: "true", //<!-- ------------ si esta en true bloquea el contenido de la web mientras la ventana esta activa (muy elegante) -->
                        buttons: {
                            Ok: function () {
                                $(this).dialog("close");
                            }
                        },
                        show: {
                            effect: "blind",
                            duration: 1000
                        },
                        hide: {
                            effect: "blind",
                            duration: 1000
                        }
                    });
                }
                
            }
		});
	});
	$("#fon_button").click(function(){
		//alert("Me has pulsado");
				 $.ajax({
            type: "GET",
            mode: 'cors',
            url: "https://api.darksky.net/forecast/8f3c5abf8f60348fe538f3f5cb849182/38.7847189,-0.860699,12?units=ca&excludeminutely,hourly,flags&lang=es",
            success: function(data, status){
                //console.log(data);

                

                if(data === "error"){
                	window.location = "index.php?page=homepage&op=list";
                }else{
                	$("#weather_api").html(data.currently.summary);
                    $("#precipProbability").html(data.currently.precipProbability + "%");
                    $("#temperature_api").html(data.currently.temperature + "ºC");
                    $("#wind_velocity").html(data.currently.windSpeed);
                    $("#wind_gust").html(data.currently.windGust);
                    $("#weather_week").html(data.daily.summary);
         
                    $("#details_weather").show();
                    $("#weather_modal").dialog({
                        width: 850, //<!-- ------------- ancho de la ventana -->
                        height: 600, //<!--  ------------- altura de la ventana -->
                        show: "scale", //<!-- ----------- animación de la ventana al aparecer -->
                        hide: "scale", //<!-- ----------- animación al cerrar la ventana -->
                        resizable: "false", //<!-- ------ fija o redimensionable si ponemos este valor a "true" -->
                        position: "center",//<!--  ------ posicion de la ventana en la pantalla (left, top, right...) -->
                        modal: "true", //<!-- ------------ si esta en true bloquea el contenido de la web mientras la ventana esta activa (muy elegante) -->
                        buttons: {
                            Ok: function () {
                                $(this).dialog("close");
                            }
                        },
                        show: {
                            effect: "blind",
                            duration: 1000
                        },
                        hide: {
                            effect: "blind",
                            duration: 1000
                        }
                    });
                }
                
            }
		});
	});
	$("#val_button").click(function(){
		//alert("Me has pulsado");
				 $.ajax({
            type: "GET",
            mode: 'cors',
            url: "https://api.darksky.net/forecast/8f3c5abf8f60348fe538f3f5cb849182/39.4079665,-0.5015961,11?units=ca&excludeminutely,hourly,flags&lang=es",
            success: function(data, status){
                //console.log(data);

                

                if(data === "error"){
                	window.location = "index.php?page=homepage&op=list";
                }else{
                	$("#weather_api").html(data.currently.summary);
                    $("#precipProbability").html(data.currently.precipProbability + "%");
                    $("#temperature_api").html(data.currently.temperature + "ºC");
                    $("#wind_velocity").html(data.currently.windSpeed);
                    $("#wind_gust").html(data.currently.windGust);
                    $("#weather_week").html(data.daily.summary);
         
                    $("#details_weather").show();
                    $("#weather_modal").dialog({
                        width: 850, //<!-- ------------- ancho de la ventana -->
                        height: 600, //<!--  ------------- altura de la ventana -->
                        show: "scale", //<!-- ----------- animación de la ventana al aparecer -->
                        hide: "scale", //<!-- ----------- animación al cerrar la ventana -->
                        resizable: "false", //<!-- ------ fija o redimensionable si ponemos este valor a "true" -->
                        position: "center",//<!--  ------ posicion de la ventana en la pantalla (left, top, right...) -->
                        modal: "true", //<!-- ------------ si esta en true bloquea el contenido de la web mientras la ventana esta activa (muy elegante) -->
                        buttons: {
                            Ok: function () {
                                $(this).dialog("close");
                            }
                        },
                        show: {
                            effect: "blind",
                            duration: 1000
                        },
                        hide: {
                            effect: "blind",
                            duration: 1000
                        }
                    });
                }
                
            }
		});
	});
});