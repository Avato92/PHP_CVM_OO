<section>
    <section>
        <section>
            <div class="center">
                <div>Datos del usuario:</div>
                <div>
                <form role="form" method="post" id="form_user" class="form_user">
                    <label>Nombre*</label>
                        <input type="text" name="user_name" id="user_name" required>
                        <span id="error_user_register"></span>
                    <br>
                    <label>Email*</label>
                        <input type="text" name="user_email" id="user_email" required>
                        <span id="error_email_register"></span>
                    <br>
                    <label>Contraseña*</label>
                        <input  type="password" name="user_password" id="user_password" required>
                        <span id="error_password_register"></span>
                    <br>
                    <label>Repita contraseña*</label>
                        <input type="password" name="user_password2" id="user_password2" required>
                        <span id="error_password2_register"></span>
                    <br>
                        <button name="submit" type="Button" id="submit_btn" onclick="validate_register();">Registrar</button>
                 </form>
                </div>
            </div>
        </section>
    </section>
</section>