<?php

        $path = $_SERVER['DOCUMENT_ROOT'] . '/Ejer10_Plantilla/';
        include ($path . "module/login/model/DAOUser.php");
        if(!isset($_SESSION)){
        session_start();
        }

         if(isset($_GET['op'])){
         	switch($_GET['op']){

        		case 'sign';
        			include("module/login/view/sign.php");
        			break;

                case 'register';
                    include("module/login/view/register.php");
                    break;

                case 'insert_into';
                	$user = $_POST['user_name'];
                	$password = $_POST['user_password'];
                	$email = $_POST['user_email'];
                    $error = array('name' => false, 'email' => false,);

                    try{
                        $daoname = new DAOUser();
                        $rdo = $daoname->find_user($user);
                        if(!$rdo){
                        $error['name'] = true;
                        exit;
                    }
                    }catch(Excepcion $e){
                    	echo json_encode("error");
                       exit;
                    }
                    try{
                    	$daoemail = new DAOUser();
                    	$rdo = $daoemail ->find_email($email);
                        if(!$rdo){
                            $error['email'] = true;
                        }
                    }catch(Excepcion $e){
                    	echo json_encode("error");
                        exit;
                    }try{
                    	$daouser = new DAOUser();
                    	$rdo = $daouser -> register_user($user, $password, $email);
                    }catch(Excepcion $e)	{
                    	echo json_encode("error");
                        exit;
                    }
                    if(!$rdo){
                    	echo json_encode($error);
                    	exit;
                    }else{
                    	echo json_encode("ok");
                    	exit;
                    }
                    break;

                case 'login';
                    $user = $_POST['user'];
                    $password = $_POST['password'];

                    try{
                        $daoname = new DAOUser();
                        $rdo = $daoname -> select_user($user);
                    }catch(Excepcion $e){
                       echo json_encode("error");
                       exit;
                    }if(!$rdo){
                        echo json_encode("error1");
                        exit;
                    }else{
                        try{
                        $daopassword = new DAOUser();
                        $check = $daopassword -> confirm_password($user, $password);
                    }catch(Excepcion $e){
                        echo json_encode("error");
                        exit;
                    }if(!$check){
                        echo json_encode("error2");
                        exit;
                    }else{
                        $user=get_object_vars($check);
                        $_SESSION['user'] = $user['user_name'];
                        $_SESSION['type'] = $user['type'];
                        echo json_encode("ok");
                        }
                    }
                    break;

                case 'log_out';
                    unset($_SESSION['user']);
                    unset($_SESSION['type']);
                    include("view/inc/menu_no_auth.php");
                    include ("module/login/view/sign.php");
                    break;
                case 'recover';
                    include("module/login/view/recover.php");
                    break;
                case 'check_recover';
                    $user = $_POST['recover_user'];
                    $_SESSION['recover_email'] = $_POST['recover_email'];
                    $email = $_POST['recover_email'];
                    $_SESSION['email'] = $email;
                    // echo json_encode($email);
                    // exit();
                    try{
                        $daorecover = new DAOUser();
                        $rdo = $daorecover -> recover_password($user, $email);
                        // echo json_encode($rdo);
                        // exit();

                    }catch(Excepcion $e){
                        echo json_encode("error");
                        exit();
                    }
                    echo json_encode($rdo);
                        exit();
                        break;
                case 'check_email';
                    $secret_number = md5(uniqid(rand(),true));
                     echo '<script language="javascript">alert("'.$secret_number.'");</script>'; 
                    // print_r($secret_number2);
                    // die();
                    $_SESSION['secret_number'] = $secret_number;
                    //$email = $_SESSION['recover_email'];
                    // mail ($email, "Confirmación cambio de contraseña", "La clave que debe introducir para cambiar de contraseña es: '$secret_number'" );
                    include("module/login/view/check_email.php");
                    break;
                case 'check_md5';
                    $md5_user = $_POST['md5'];
                    // echo json_encode($md5_user);
                    // exit();
                    $md5_server = $_SESSION['secret_number'];
                    // echo json_encode($md5_server);
                    // exit();
                    if($md5_user == $md5_server){
                        $error = 0;
                        echo json_encode($error);
                        exit();
                    }else{
                        $error = 1;
                        echo json_encode($error);
                        exit();
                    }
                    break;
                case 'update_password';
                    include("module/login/view/update_password.php");
                    break;
                case 'DAO_update_password';
                    $password = $_POST['new_password'];
                    $email = $_SESSION['email'];
                    // echo json_encode($email);
                    // exit();
                    try{
                        $daoemail = new DAOUser();
                        $rdo = $daoemail -> update_password($email, $password);
                        // echo json_encode($rdo);
                        // exit();

                    }catch(Excepcion $e){
                        echo json_encode("error");
                        exit();
                    }
                    echo json_encode($rdo);
                    exit();
                    break;
                default;
                    include("module/login/view/sign.php");
                    break;
                }
            }