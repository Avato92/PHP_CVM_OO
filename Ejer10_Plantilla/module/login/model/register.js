function validate_register(){
    
     //alert("Dentro");
    var check =true;
    var user = document.getElementById('user_name').value;
    var email = document.getElementById('user_email').value;
    var password = document.getElementById('user_password').value;
    var password2 = document.getElementById('user_password2').value;

    //alert (email);


    if(user === ""){
    	document.getElementById('error_user_register').innerHTML = "Introduce el usuario";
    	$("input#user_name").focus();
    	check = false;
    }else{
    	document.getElementById('error_user_register').innerHTML ="";
    }


    if(email === ""){
    	document.getElementById('error_email_register').innerHTML = "Introduzca el email";
    	$("input#user_email").focus();
    	check = false;
    }else{
    	document.getElementById('error_email_register').innerHTML ="";
    }


    if(password === ""){
    	document.getElementById('error_password_register').innerHTML = "Introduce la contraseña";
    	$("input#user_password").focus();
    	check = false;
    }else{
    	document.getElementById('error_password_register').innerHTML =""
    }

    if(password2 === ""){
    	document.getElementById('error_password2_register').innerHTML = "Introduzca de nuevo la contraseña";
    	$("input#user_password2").focus();
    	check = false;
    }else{
   		document.getElementById('error_password2_register').innerHTML = "";
    }

    
    if(password != password2){
    	document.getElementById('error_password2_register').innerHTML = "Las contraseñas deben coincidir";
    	$("input#user_password2").focus();
    	check = false;
    }

    //document.getElementById('form_user').submit();
    return check;
 
    }

$(document).ready(function(){
    //alert("Hola");
    $("#submit_btn").click(function(){
        //alert("Hola2");
        
        if(validate_register()){
         $.ajax({
             type: "POST",
             url: "module/login/controller/controller_login.php?op=insert_into",
             data: $("#form_user").serialize(),
             success: function(data, status){
                //console.log(data);
                var json = JSON.parse(data);
                // if(json.email){
                //     alert("dentro");
                // }

                if(json === "ok"){
                alert("Usuario registrado correctamente");
                window.location = "index.php?page=homepage&op=list";
                }else if(json.name){
                    document.getElementById('error_user_register').innerHTML = "Usuario ya existente";
                }else if(json.email){
                    document.getElementById('error_email_register').innerHTML = "Email en uso";
                }else if(json === "error"){
                    alert("No ha sido posible registrar al usuario, por favor intentelo más tarde");
                }
            }
        });}
    });
});