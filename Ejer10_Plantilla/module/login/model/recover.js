function validate_recover(){
	var user = document.getElementById('recover_user').value;
	var email = document.getElementById('recover_email').value;
	var check = true;
	// alert(user);
	// alert(email);
	if (user === ""){
		document.getElementById('error_recover_user').innerHTML = "Introduce nombre de usuario";
		check = false;
	}else{
		document.getElementById('error_recover_user').innerHTML = "";
	}if (email === ""){
		document.getElementById('error_recover_email').innerHTML = "Introduce el email";
		check = false;
	}else{
		document.getElementById('error_recover_email').innerHTML = "";
	}
	return check;
};

$(document).ready(function(){
	//alert("Hola soy recover");
	$("#check_recover").click(function(){
		//console.log("Funciono");

		if(validate_recover()){
			//alert("Todo correcto");
			$.ajax({
            type: "POST",
            url: "module/login/controller/controller_login.php?op=check_recover",
            data: $("#recover").serialize(),
            success: function(data, status){
            	console.log(data);
            	if (data == 0){
            		//alert("No hay problema");
            		document.getElementById('error_recover_email').innerHTML= "";
            		document.getElementById('error_recover_user').innerHTML="";
            		location.href="index.php?page=controller_login&op=check_email";
            	}else if(data == 1){
            		document.getElementById('error_recover_email').innerHTML= "Email erroneo";
            		document.getElementById('error_recover_user').innerHTML="";
            		//alert("Email erroneo");
            	}else if(data == 2){
            		document.getElementById('error_recover_email').innerHTML= "";
            		document.getElementById('error_recover_user').innerHTML="Usuario erroneo";
            		//alert("Usuario erroneo");
            	}else if(data == 3){
            		document.getElementById('error_recover_email').innerHTML= "Email erroneo";
            		document.getElementById('error_recover_user').innerHTML="Usuario erroneo";
            		//alert("Usuario y email no encontrados");
            	}else{
            		alert("Ha fallado algo");
            	}
            	}
        	});
		}
	});
      $('#key').click(function(){
            //alert("Funciono");
            var key = document.getElementById('md5').value;
            //alert(key);
            if(key == ""){
                  document.getElementById('error_md5').innerHTML = "Introduce la clave";
            }else{
                  document.getElementById('error_md5').innerHTML = "";
                  $.ajax({
            type: "POST",
            url: "module/login/controller/controller_login.php?op=check_md5",
            data: {md5 : key},
            success: function(data, status){
                  //console.log(data);
                  if(data == 0){
                        alert("Clave correcta");
                        location.href="index.php?page=controller_login&op=update_password";
                        }else{
                              alert("Clave incorrecta");
                        }
                  }

                  });
            }
      });
       $('#confirm_update_password').click(function(){
            $error = true;
             // alert("Funciono");
            var password1 = document.getElementById('update_password1').value;
            var password2 = document.getElementById('update_password2').value;

            if(password1 == ""){
                  document.getElementById('error_update_password1').innerHTML= "Introduce la nueva contraseña";
                  $error = false;
            }else{
                  document.getElementById('error_update_password1').innerHTML="";
            }if (password2 == ""){
                  document.getElementById('error_update_password2').innerHTML= "Introduce de nuevo la contraseña";
                  $error = false;
            }else{
                  document.getElementById('error_update_password2').innerHTML="";
            }if((password1 != password2) && (password1 != "") && (password2 != "")){
                  document.getElementById('error_update_password2').innerHTML="Las contraseñas deben coincidir";
                  $error = false;
            }if ($error == true){
                   $.ajax({
            type: "POST",
            url: "module/login/controller/controller_login.php?op=DAO_update_password",
            data: {new_password : password1},
            success: function(data,status){
                  //alert(data);
                  if (data == 0){
                        alert("Contraseña modificada con éxito");
                        location.href="index.php?page=controller_login&op=sign";
                  }else{
                        alert("No se ha podido modificar la contraseña");
                        }
                  }
             });
            }
       });
});