<section class="center">
<section>
<section class="center">
<div id="contenido">
  <?php
        $error_weapon = "";
        $error_checkbox = "";
        ?>
	<form method = "post" name="form_weapon" id="form_weapon" action="index.php?page=controller_weapon&op=create">
	<p>
		<label for="weapon_name"><?php echo $create_form['weapon_name']?></label>
		<input name="weapon_name" class="weapon_name" type="text" placeholder="<?php echo $create_form['name_placeholder']?>" value="">
		<span id="error_name_weapon" class="error">
    <?php 
    echo "$error_weapon";
    ?>  
    </span>
	</p>
	<p>
		<label for="caliber"><?php echo $create_form['caliber']?></label>
		<select name="caliber" id="caliber">
              <option value="7,62mm x 51mm">7,62mm x 51mm</option>
              <option value="5,56mm x 45mm">5,56mm x 45mm</option>
              <option value="12,70mm x 99mm">12,70mm x 99mm</option>
              <option value="Otro"><?php echo $create_form['another']?></option>
        </select>
    </p>
    <p>
    	<label for="country"><?php echo $create_form['country']?></label>
    		    EEUU<input name="country" class="country" type="radio" value="EEUU" checked>
            Rusia<input name="country" class="country" type="radio" value="Rusia">
            Alemania<input name="country" class="country" type="radio" value="Alemania">
            <?php echo $create_form['another']?><input name="country" class="country" type="radio" value="Otro">
    </p>
    <p>
    	<label for="datepicker"><?php echo $create_form['date']?></label>
    		<input type="text" id="datepicker" name="datepicker" placeholder="<?php echo $create_form['date_placeholder']?>" readonly="readonly">
        <span id="error_datepicker" class="error"></span>
   	</p>
   	<p>
   		<label for="complements"><?php echo $create_form['complements']?></label>
   			<?php echo $create_form['telescopic_sight']?><input name="complement[]" id="complement" class="complement[]" type="checkbox" value="Mira telescopica" checked>
            <?php echo $create_form['cartridge_belts']?><input name="complement[]" id="complement" class="complement[]" type="checkbox" value="Cananas y cartucheras">
            <br>
   		    Bípode<input name="complement[]" id="complement" class="complement[]" type="checkbox" value="Bipode">
            Trípode<input name="complement[]" id="complement" class="complement[]" type="checkbox" value="Tripode">
            Otros<input name="complement[]" id="complement" class="complement[]" type="checkbox" value="Otros">
            <span id="error_complements" class="error">
              <?php
              echo "$error_checkbox";
              ?>
            </span>
    </p>
    <p>
    	<label for="description">Descripción del arma</label>
    	<br>
    		<textarea rows=10 cols=50 name="description" class="description" placeholder="Escriba aquí una pequeña descripción del arma" value=""></textarea>
    		<span id="error_description" class="error"></span>
    </p>
    		<input name="Submit" type="button" value="Guardar" onclick="validate_weapon()" />
        <td align="right"><a href="index.php?page=controller_weapon&op=list">Volver</a></td>
</form>
</div>
</section>
</section>
</section>