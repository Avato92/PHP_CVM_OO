<div id="contenido">
    <h1>Informacion del Arma</h1>
    <p>
    <table border='2'>
        <tr>
            <td>Arma: </td>
            <td>
                <?php
                    echo $weapon['weapon_name'];
                ?>
            </td>
        </tr>
    
        <tr>
            <td>Calibre: </td>
            <td>
                <?php
                    echo $weapon['caliber'];
                ?>
            </td>
        </tr>
        
        <tr>
            <td>País: </td>
            <td>
                <?php
                    echo $weapon['country'];
                ?>
            </td>
        </tr>
        
        <tr>
            <td>Fecha de creación: </td>
            <td>
                <?php
                    echo $weapon['datepicker'];
                ?>
            </td>
        </tr>
        
        <tr>
            <td>Complementos: </td>
            <td>
                <?php
                    echo $weapon['complements'];
                ?>
            </td>
        </tr>
        
        <tr>
            <td>Descripción: </td>
            <td>
                <?php
                    echo $weapon['description'];
                ?>
            </td>
    </table>
    </p>
    <p><a href="index.php?page=controller_weapon&op=list">Volver</a></p>
</div>