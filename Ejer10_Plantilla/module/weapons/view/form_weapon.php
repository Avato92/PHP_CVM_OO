<html>
<body>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Formulario</title>
  <link href="style.css" rel="stylesheet" type="text/css"/>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#datepicker" ).datepicker({ dateFormat: 'dd/mm/yy', changeMonth: true, changeYear: true, yearRange: '1900:2018',});
  } );
  </script>
  <script type="text/javascript" src="validate_weapon.js"></script>


  <form name="form_weapon" class="form" method="POST" action="results.php" onsubmit="return validate_form()">
  <p>
    <label for="weapon_name">Nombre del arma:</label>
    <input name="weapon_name" id="weapon_name" type="text" placeholder="Escriba el nombre del arma">
    <span id="error_name_weapon" class="styerror"></span>
  </p>
  <p>
    <label for="caliber">Calibre del arma:</label>
    <select name="caliber" id="caliber">
      <option value="7,62mm x 59mm">7,62mm x 59mm</option>
      <option value="5,56mm x 45mm">5,56mm x 45mm</option>
      <option value="12,70mm x 99mm">12,70mm x 99mm</option>
      <option value="Otro">Otro</option>
    </select>
  </p>
  <p>
    <label for="country">País de origen:</label>
      EEUU<input name="country" class="country" type="radio" value="EEUU" checked>
      Alemania<input name="country" class="country" type="radio" value="Alemania">
      Rusia<input name="country" class="country" type="radio" value="Rusia">
      Otro<input name="country" class="country" type="radio" value="Otro">
  </p>
  <p>
    <label for="datepicker">Año de creación:</label>
    <input type="text" id="datepicker" name="datepicker" placeholder="Elija la fecha" readonly>
  </p>
  <p>
    <label for="complements">Complementos:<br></label>
            Mira telescópica<input name="complement[]" class="complement[]" type="checkbox" value="Mira telescopica" checked>
            Cananas y cartucheras<input name="complement[]" class="complement[]" type="checkbox" value="Cananas y cartucheras">
            <br>
            Bípode<input name="complement[]" class="complement[]" type="checkbox" value="Bipode">
            Trípode<input name="complement[]" class="complement[]" type="checkbox" value="Tripode">
            Otros<input name="complement[]" class="complement[]" type="checkbox" value="Otros">
  </p>
  <p>
    <label for="description">Descripción del arma</label>
    <br>
            <textarea rows=10 cols=50 name="description" class="description" placeholder="Escriba aquí una pequeña descripción del arma" value=""></textarea>
            <span id="error_description" class="styxerror"></span>
  </p>
        <input name="Submit" type="Submit" value="Guardar"/>
</form>