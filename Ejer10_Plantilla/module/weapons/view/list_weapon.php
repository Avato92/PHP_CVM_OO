<section class="center">
<div id="contenido">
    <div class="container">
    	<div class="row">
    			<h3>LISTA DE ARMAS</h3>
    	</div>
    	<div class="row">
    		<p><a href="index.php?page=controller_weapon&op=create"><img src="view/img/anadir.png"></a></p>
    		
    		<table>
                <tr>
                    <td width=125><b>Nombre arma</b></td>
                    <td width=125><b>Caliber</b></td>
                    <td width=125><b>País</b></td>
                    <td width=350><b>Accion</b></td>
                </tr>
                <?php
                    if ($rdo->num_rows === 0){
                        echo '<tr>';
                        echo '<td align="center"  colspan="3">NO HAY NINGUN USUARIO</td>';
                        echo '</tr>';
                    }else{
                        foreach ($rdo as $row) {
                       		echo '<tr>';
                    	   	echo '<td width=125>'. $row['weapon_name'] . '</td>';
                    	   	echo '<td width=125>'. $row['caliber'] . '</td>';
                    	   	echo '<td width=125>'. $row['country'] . '</td>';
                    	   	echo '<td width=350>';

                            print ("<div class='Button_blue' id='".$row['weapon_name']."'>Read</div>");
                            echo '&nbsp;';
                    	   	// echo '<a class="Button_blue" href="index.php?page=controller_weapon&op=read&id='.$row['weapon_name'].'">Read</a>';
                    	   	// echo '&nbsp;';
                    	   	echo '<a class="Button_green" href="index.php?page=controller_weapon&op=update&id='.$row['weapon_name'].'">Update</a>';
                    	   	echo '&nbsp;';
                    	   	echo '<a class="Button_red" href="index.php?page=controller_weapon&op=delete&id='.$row['weapon_name'].'">Delete</a>';
                    	   	echo '</td>';
                    	   	echo '</tr>';
                        }
                    }
                ?>
            </table>
    	</div>
    </div>
</div>
</section>
<!-- modal window -->
<section id="weapon_modal">
    <section>
    <div id="details_weapon" hidden>
        <div id="details">
            <div id="container">
                        Nombre del arma: <div id="weapon_name"></div></br>
                        Calibre: <div id="caliber"></div></br>
                        País de origen: <div id="country"></div></br>
                        Año de creación: <div id="datepicker"></div></br>
                        Complementos: <div id="complements"></div></br>
                        Descripción: <div id="description"></div></br>
            </div>
        </div>
    </div>
</section>
</section>
