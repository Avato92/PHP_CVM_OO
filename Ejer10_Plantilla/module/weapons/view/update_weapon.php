<section class="center">
<div id="contenido">
  <?php
        $error_weapon = "";
        $error_checkbox = "";
        ?>
	<form method = "post" name="form_weapon" id="form_weapon" action="index.php?page=controller_weapon&op=update" onsubmit="return validate();">
	<p>
		<label for="weapon_name">Nombre del arma:</label>
		<input name="weapon_name" class="weapon_name" type="text" value="<?php echo $weapon['weapon_name'];?>" readonly/>
		<span id="error_name_weapon" class="error">
    <?php 
    echo "$error_weapon";
    ?>  
    </span>
	</p>


	<p>
		<label for="caliber">Calibre:</label>
    <?php
    if ($weapon['caliber'] === "7,62mm x 51mm"){
      ?>
    <select name="caliber" id="caliber">
              <option value="7,62mm x 51mm" selected>7,62mm x 51mm</option>
              <option value="5,56mm x 45mm">5,56mm x 45mm</option>
              <option value="12,70mm x 99mm">12,70mm x 99mm</option>
              <option value="Otro">Otro</option>
        </select>
  <?php
  }elseif ($weapon['caliber'] === "5,56mm x 45mm"){
    ?>
    <select name="caliber" id="caliber">
              <option value="7,62mm x 51mm">7,62mm x 51mm</option>
              <option value="5,56mm x 45mm" selected>5,56mm x 45mm</option>
              <option value="12,70mm x 99mm">12,70mm x 99mm</option>
              <option value="Otro">Otro</option>
        </select>
    <?php
     }elseif ($weapon['caliber'] === "12,70mm x 99mm"){
      ?>
    <select name="caliber" id="caliber">
              <option value="7,62mm x 51mm">7,62mm x 51mm</option>
              <option value="5,56mm x 45mm">5,56mm x 45mm</option>
              <option value="12,70mm x 99mm" selected>12,70mm x 99mm</option>
              <option value="Otro">Otro</option>
        </select>
    <?php
    }else{
      ?>
    <select name="caliber" id="caliber">
              <option value="7,62mm x 51mm">7,62mm x 51mm</option>
              <option value="5,56mm x 45mm">5,56mm x 45mm</option>
              <option value="12,70mm x 99mm">12,70mm x 99mm</option>
              <option value="Otro" selected>Otro</option>
        </select>
    <?php
      }
      ?>
      </p>


    <p>
    	<label for="country">País de origen:</label>
      <?php
      if ($weapon['country'] == "EEUU"){
        ?>
            EEUU<input name="country" class="country" type="radio" value="EEUU" checked>
            Rusia<input name="country" class="country" type="radio" value="Rusia">
            Alemania<input name="country" class="country" type="radio" value="Alemania">
            Otro<input name="country" class="country" type="radio" value="Otro">
      <?php
      }elseif ($weapon['country'] === "Rusia"){
        ?>
            EEUU<input name="country" class="country" type="radio" value="EEUU">
            Rusia<input name="country" class="country" type="radio" value="Rusia" checked>
            Alemania<input name="country" class="country" type="radio" value="Alemania">
            Otro<input name="country" class="country" type="radio" value="Otro">
      <?php
      }elseif ($weapon['country'] === "Alemania"){
        ?>
            EEUU<input name="country" class="country" type="radio" value="EEUU">
            Rusia<input name="country" class="country" type="radio" value="Rusia">
            Alemania<input name="country" class="country" type="radio" value="Alemania" checked>
            Otro<input name="country" class="country" type="radio" value="Otro">
      <?php
      }else{
        ?>
            EEUU<input name="country" class="country" type="radio" value="EEUU">
            Rusia<input name="country" class="country" type="radio" value="Rusia">
            Alemania<input name="country" class="country" type="radio" value="Alemania">
            Otro<input name="country" class="country" type="radio" value="Otro" checked>
      <?php
    }
    ?>
      </p>


      <p>
        <label for="datepicker">Fecha de creación:</label>
        <input type="text" id="datepicker" name="datepicker" placeholder="Elija la fecha" readonly="readonly" value= "<?php echo $weapon['datepicker'];?>">
        <span id="error_datepicker" class="error"></span>
      </p>


      <p>
      <label for="complements">Complementos:</label>
                <?php
                    $complement=explode("-", $weapon['complements']);
                    $find_complement=in_array("Mira telescopica", $complement);
                      if($find_complement){
                ?>
                    Mira telescópica<input name="complement[]" id="complement" class="complement[]" type="checkbox" value="Mira telescopica" checked>

                <?php 
                      }else{
                ?>
                  Mira telescópica<input name="complement[]" id="complement" class="complement[]" type="checkbox" value="Mira telescopica" >
        <?php
                      }
                    $find_complement=in_array("Cananas y cartucheras", $complement);
                      if($find_complement){
        ?>
                  Cananas y cartucheras<input name="complement[]" id="complement" class="complement[]" type="checkbox" value="Cananas y cartucheras" checked>
        <?php
                      }else{
        ?>
                          Cananas y cartucheras<input name="complement[]" id="complement" class="complement[]" type="checkbox" value="Cananas y cartucheras">
                    <br>
        <?php              
                      }
                    $find_complement=in_array("Bipode", $complement);
                      if($find_complement){
        ?>
                          Bípode<input name="complement[]" id="complement" class="complement[]" type="checkbox" value="Bipode" checked>
        <?php
                        }else{
        ?>
                          Bípode<input name="complement[]" id="complement" class="complement[]" type="checkbox" value="Bipode">
        <?php
                      }
                      $finde_complement=in_array("Tripode", $complement);
                        if($find_complement){
        ?>
                          Trípode<input name="complement[]" id="complement" class="complement[]" type="checkbox" value="Tripode" checked>
        <?php
                        }else{
        ?>
                          Trípode<input name="complement[]" id="complement" class="complement[]" type="checkbox" value="Tripode">
        <?php
                        }
                        $find_complement=in_array("Otros", $complement);
                          if($complement){
        ?>
                          Otros<input name="complement[]" id="complement" class="complement[]" type="checkbox" value="Otros" checked>
        <?php
                          }else{
        ?>
                          Otros<input name="complement[]" id="complement" class="complement[]" type="checkbox" value="Otros">
        <?php
                          }
        ?>
            <span id="error_complements" class="error">
              <?php
              echo "$error_checkbox";
              ?>
            </span>
    </p>


        <p>
      <label for="description">Descripción del arma</label>
      <br>
        <textarea rows=10 cols=50 name="description" class="description" placeholder="Escriba aquí una pequeña descripción del arma"><?php echo $weapon['description'];?></textarea>
        <span id="error_description" class="error"></span>
    </p>
        <input name="Submit" type="button" value="Guardar" onclick="validate_weapon()" />
        <td align="right"><a href="index.php?page=controller_weapon&op=list">Volver</a></td>
</form>
</div>
</section>