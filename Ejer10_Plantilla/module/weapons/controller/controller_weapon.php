<?php
    
    $path = $_SERVER['DOCUMENT_ROOT'] . '/Ejer10_Plantilla/';
    include ($path . "module/weapons/model/DAOWeapon.php");

    if(!isset($_SESSION)){
    session_start();
    }

    if(isset($_SESSION['type']) && ($_SESSION['type'] != 2)){
        include("view/inc/error_user.php");
    }else{

    switch($_GET['op']){
        case 'list';
            try{
                $daoweapon = new DAOWeapon();
            	$rdo = $daoweapon->select_all_weapon();
            }catch (Exception $e){
                $callback = 'index.php?page=503';
			    die('<script>window.location.href="'.$callback .'";</script>');
            }
            
            if(!$rdo){
    			$callback = 'index.php?page=503';
			    die('<script>window.location.href="'.$callback .'";</script>');
    		}else{
                include("module/weapons/view/list_weapon.php");
    		}
            break;
            
        case 'create';
            include("module/weapons/model/validate.php");
            $val = true;
            
            if ($_POST){
                // HAY QUE ARREGLAR EL VALIDATE.PHP, hace fallar al CREATE
                //$val=validate();
                
                if ($val){
                    $_SESSION['weapon']=$_POST;
                    try{
                        $daoweapon = new DAOWeapon();
    		            $rdo = $daoweapon->insert_weapon($_POST);
                    }catch (Exception $e){
                        $callback = 'index.php?page=503';
        			    die('<script>window.location.href="'.$callback .'";</script>');
                    }
                    
		            if($rdo){
            			$callback = 'index.php?page=controller_weapon&op=list';
        			    die('<script>window.location.href="'.$callback .'";</script>');
            		}else{ 
            			$callback = 'index.php?page=504';
    			        die('<script>window.location.href="'.$callback .'";</script>');
            		}
                }
            }
            include("module/weapons/view/create_weapon.php");
            break;
            
        case 'update';

            include("module/weapons/model/validate.php");
         
            $val = true;
            
            if ($_POST){
                //$val=validate();
                
                if ($val){
                    $_SESSION['weapon']=$_POST;
                    try{
                        $daoweapon = new DAOWeapon();
    		            $rdo = $daoweapon->update_weapon($_POST);
                    }catch (Exception $e){                    
                        $callback = 'index.php?page=503';
        			    die('<script>window.location.href="'.$callback .'";</script>');
                    }
                    
		            if($rdo){
            			$callback = 'index.php?page=controller_weapon&op=list';
        			    die('<script>window.location.href="'.$callback .'";</script>');
            		}else{
            			$callback = 'index.php?page=503';
    			        die('<script>window.location.href="'.$callback .'";</script>');
            		}
                }
            }
            
            try{
                $daoweapon = new DAOWeapon();
            	$rdo = $daoweapon->select_weapon($_GET['id']);
            	$weapon=get_object_vars($rdo);
            }catch (Exception $e){
                $callback = 'index.php?page=503';
			    die('<script>window.location.href="'.$callback .'";</script>');
            }
            
            if(!$rdo){
    			$callback = 'index.php?page=503';
    			die('<script>window.location.href="'.$callback .'";</script>');
    		}else{
        	    include("module/weapons/view/update_weapon.php");
    		}
            break;
            
        case 'read';
            try{
                $daoweapon = new DAOWeapon();
            	$rdo = $daoweapon->select_weapon($_GET['id']);
            	$weapon=get_object_vars($rdo);
            }catch (Exception $e){
                $callback = 'index.php?page=503';
			    die('<script>window.location.href="'.$callback .'";</script>');
            }
            if(!$rdo){
    			$callback = 'index.php?page=503';
    			die('<script>window.location.href="'.$callback .'";</script>');
    		}else{
                include("module/weapons/view/read_weapon.php");
    		}
            break;
            
        case 'delete';
            if (isset($_POST['delete'])){
                try{
                    $daoweapon = new DAOWeapon();
                	$rdo = $daoweapon->delete_weapon($_GET['id']);
                }catch (Exception $e){
                    $callback = 'index.php?page=503';
    			    die('<script>window.location.href="'.$callback .'";</script>');
                }
            	
            	if($rdo){
        			$callback = 'index.php?page=controller_weapon&op=list';
    			    die('<script>window.location.href="'.$callback .'";</script>');
        		}else{
        			$callback = 'index.php?page=503';
			        die('<script>window.location.href="'.$callback .'";</script>');
        		}
            }
            
            include("module/weapons/view/delete_weapon.php");
            break;

        case 'read_modal':

            try{
                $daoweapon = new DAOWeapon();
                $rdo = $daoweapon->select_weapon($_GET['modal']);
            }catch (Exception $e){
                echo json_encode("error");
                exit;
            }
            if(!$rdo){
                echo json_encode("error");
                exit;
            }else{

                $weapon=get_object_vars($rdo);
                echo json_encode($weapon);
                exit;
            }
            break;
        default;
            include("view/inc/error404.php");
            break;
    }
}