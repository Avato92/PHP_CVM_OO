<?php

    $path = $_SERVER['DOCUMENT_ROOT'] . '/Ejer10_Plantilla/';
    include($path . "model/connect.php");
    
	class DAOWeapon{
		function insert_weapon($datos){
			$weapon_name=$datos['weapon_name'];
        	$caliber=$datos['caliber'];
        	$country=$datos['country'];
        	$datepicker=$datos['datepicker'];
            $complements="";
        	foreach ($datos['complement'] as $indice) {
        	    $complements=$complements."$indice - ";
        	}
            $description=$datos['description'];
            $conexion = connect::con();
            $find = mysqli_query($conexion,"SELECT weapon_name FROM weapons WHERE weapon_name='$weapon_name'");
                if(mysqli_num_rows($find)>0){
                    $res = false;
                    return $res;
                }else{
        	$sql = " INSERT INTO weapons (weapon_name, caliber, country, datepicker, complements, description)"
        		. " VALUES ('$weapon_name', '$caliber', '$country', '$datepicker', '$complements', '$description')";
            
            //$conexion = connect::con();
            $res = mysqli_query($conexion, $sql);
            connect::close($conexion);
			return $res;
		}}
		
		function select_all_weapon(){
			$sql = "SELECT * FROM weapons";
			
			$conexion = connect::con();
            $res = mysqli_query($conexion, $sql);
            connect::close($conexion);
            return $res;
		}
        
		
		function select_weapon($weapons){
			$sql = "SELECT * FROM weapons WHERE weapon_name='$weapons'";
			
			$conexion = connect::con();
            $res = mysqli_query($conexion, $sql)->fetch_object();
            connect::close($conexion);
            return $res;
		}
		
		function update_weapon($datos){
			$weapon_name=$datos['weapon_name'];
        	$caliber=$datos['caliber'];
        	$country=$datos['country'];
        	$datepicker=$datos['datepicker'];
        	foreach ($datos['complement'] as $indice) {
        	    $complements=$complements."$indice-";
        	}
            $description=$datos['description'];
        	
        	$sql = " UPDATE weapons SET caliber='$caliber', country='$country', datepicker='$datepicker', complements='$complements', description='$description' WHERE weapon_name='$weapon_name'";
            
            $conexion = connect::con();
            $res = mysqli_query($conexion, $sql);
            connect::close($conexion);
			return $res;
		}
		
		function delete_weapon($weapon){
			$sql = "DELETE FROM weapons WHERE weapon_name='$weapon'";
			
			$conexion = connect::con();
            $res = mysqli_query($conexion, $sql);
            connect::close($conexion);
            return $res;
		}
	}