<?php
    function validate_weapon_name($name){
        return true;
        $ex_name="/(^[A-Z][\w\d.-]+)/";
        return preg_match($ex_name,$name);
    }
    function validate_checkbox(){
        $selected = "";
        $val = true;
        foreach ($_POST['complement'] as $key => $value) {
            $selected .= $value;
        }if ($selected == ""){
                $val = false;
        }return $val;
    }

    function validate(){
    	$val = true;
        $t_weapon_name="";
        $c_weapon_name= "";
        $c_checkbox= "";

    	$t_weapon_name = $_POST ['weapon_name'];

    	$c_weapon_name = validate_weapon_name($t_weapon_name);
        $c_checkbox = validate_checkbox();

    	if($c_weapon_name !== 1){
    		$error_weapon = "El nombre del arma no es correcto";
    		$val = false;
    	}else{
    		$error_weapon = "";
    	}if($c_checkbox == false){
            $error_checkbox = "Tienes que seleccionar mínimo un complemento";
            $val = false;
        }else{
            $error_checkbox = "";
        }return $val;
    }
