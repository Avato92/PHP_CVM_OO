-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 12-03-2018 a las 17:36:29
-- Versión del servidor: 5.5.24-log
-- Versión de PHP: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `army`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `city`
--

CREATE TABLE IF NOT EXISTS `city` (
  `id` int(11) NOT NULL,
  `city_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `city`
--

INSERT INTO `city` (`id`, `city_name`) VALUES
(1, 'Ontinyent'),
(2, 'Bocairent'),
(3, 'Agullent'),
(4, 'Albaida'),
(5, 'Olleria'),
(6, 'Xativa'),
(7, 'Aielo'),
(8, 'Fontanars');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dummies`
--

CREATE TABLE IF NOT EXISTS `dummies` (
  `weapon_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `caliber` varchar(255) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `country` varchar(255) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `date` varchar(255) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `complements` varchar(255) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `dummies`
--

INSERT INTO `dummies` (`weapon_name`, `caliber`, `country`, `date`, `complements`, `description`, `image`) VALUES
('AK-47', '7,62mm x 59mm', 'Rusia', '12/12/1936', 'Mira telescopica', 'Arma rusa capaz de matar a 800 metros de distancia', 'ak47.png'),
('M-16', '5,56mm x 45mm', 'EEUU', '12/12/1936', 'Mira telescopica', 'Arma rusa capaz de matar a 800 metros de distancia', 'm16.png'),
('M-4', '5,56mm x 45mm', 'EEUU', '12/12/1936', 'Mira telescopica', 'Arma rusa capaz de matar a 800 metros de distancia', 'm4.png'),
('HK-G36', '5,56mm x 45mm', 'Alemania', '12/12/1936', 'Mira telescopica', 'Arma rusa capaz de matar a 800 metros de distancia', 'g36.png');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `favorites`
--

CREATE TABLE IF NOT EXISTS `favorites` (
  `user` varchar(255) NOT NULL,
  `like_date` varchar(255) NOT NULL,
  `item` varchar(255) NOT NULL,
  PRIMARY KEY (`user`,`item`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `favorites`
--

INSERT INTO `favorites` (`user`, `like_date`, `item`) VALUES
('Yester', '2018-03-12 15:57:08', 'AK-47'),
('Yester', '2018-03-12 16:04:08', 'AWM'),
('Yester', '2018-03-12 17:00:20', 'Chaleco de proteccion'),
('Yester', '2018-03-12 17:21:22', 'Granada alhambra'),
('Yester', '2018-03-12 16:01:03', 'SCAR-L'),
('Yester', '2018-03-12 17:01:37', 'Tommy Gun');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `invoice_line`
--

CREATE TABLE IF NOT EXISTS `invoice_line` (
  `id_line` int(10) NOT NULL,
  `id_invoice` int(10) NOT NULL,
  `product` varchar(20) NOT NULL,
  `quantity` varchar(10) NOT NULL,
  `price` varchar(10) NOT NULL,
  `total` varchar(255) NOT NULL,
  PRIMARY KEY (`id_line`,`id_invoice`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `invoice_line`
--

INSERT INTO `invoice_line` (`id_line`, `id_invoice`, `product`, `quantity`, `price`, `total`) VALUES
(1, 52, 'SCAR-L', '1', '120', '120'),
(1, 56, 'SCAR-L', '1', '120', '120'),
(1, 66, 'SCAR-L', '1', '120', '120'),
(1, 69, 'SCAR-L', '1', '120', '120'),
(1, 75, 'Chaleco de proteccio', '2', '150', '300'),
(1, 76, 'SCAR-L', '3', '120', '360'),
(1, 77, 'SCAR-L', '2', '120', '240'),
(2, 52, 'SCAR-L', '1', '120', '120'),
(2, 75, 'SCAR-L', '2', '120', '240'),
(3, 52, 'SCAR-L', '1', '120', '120'),
(4, 52, 'SCAR-L', '1', '120', '120');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `items`
--

CREATE TABLE IF NOT EXISTS `items` (
  `item_name` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `country` varchar(60) NOT NULL,
  `description` varchar(255) NOT NULL,
  `shopID` varchar(255) NOT NULL,
  `itemID` varchar(255) NOT NULL,
  `codigo` varchar(255) NOT NULL,
  `price` varchar(25) NOT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `items`
--

INSERT INTO `items` (`item_name`, `type`, `country`, `description`, `shopID`, `itemID`, `codigo`, `price`) VALUES
('SCAR-L', 'Arma', 'EEUU', 'Rifle de asalto americano', '1', '1', '1', '120'),
('SCAR-L', 'Arma', 'EEUU', 'Rifle de asalto americano', '10', '1', '10', '120'),
('SCAR-L', 'Arma', 'EEUU', 'Rifle de asalto americano', '11', '1', '11', '120'),
('SCAR-L', 'Arma', 'EEUU', 'Rifle de asalto americano', '12', '1', '12', '120'),
('SCAR-L', 'Arma', 'EEUU', 'Rifle de asalto americano', '13', '1', '13', '120'),
('SCAR-L', 'Arma', 'EEUU', 'Rifle de asalto americano', '14', '1', '14', '120'),
('SCAR-L', 'Arma', 'EEUU', 'Rifle de asalto americano', '15', '1', '15', '120'),
('SCAR-L', 'Arma', 'EEUU', 'Rifle de asalto americano', '16', '1', '16', '120'),
('SCAR-L', 'Arma', 'EEUU', 'Rifle de asalto americano', '17', '1', '17', '120'),
('SCAR-L', 'Arma', 'EEUU', 'Rifle de asalto americano', '18', '1', '18', '120'),
('SCAR-L', 'Arma', 'EEUU', 'Rifle de asalto americano', '19', '1', '19', '120'),
('SCAR-L', 'Arma', 'EEUU', 'Rifle de asalto americano', '2', '1', '2', '120'),
('SCAR-L', 'Arma', 'EEUU', 'Rifle de asalto americano', '20', '1', '20', '120'),
('SCAR-L', 'Arma', 'EEUU', 'Rifle de asalto americano', '21', '1', '21', '120'),
('SCAR-L', 'Arma', 'EEUU', 'Rifle de asalto americano', '22', '1', '22', '120'),
('SCAR-L', 'Arma', 'EEUU', 'Rifle de asalto americano', '23', '1', '23', '120'),
('SCAR-L', 'Arma', 'EEUU', 'Rifle de asalto americano', '24', '1', '24', '120'),
('SCAR-L', 'Arma', 'EEUU', 'Rifle de asalto americano', '25', '1', '25', '120'),
('SCAR-L', 'Arma', 'EEUU', 'Rifle de asalto americano', '26', '1', '26', '120'),
('SCAR-L', 'Arma', 'EEUU', 'Rifle de asalto americano', '27', '1', '27', '120'),
('SCAR-L', 'Arma', 'EEUU', 'Rifle de asalto americano', '28', '1', '28', '120'),
('SCAR-L', 'Arma', 'EEUU', 'Rifle de asalto americano', '29', '1', '29', '120'),
('SCAR-L', 'Arma', 'EEUU', 'Rifle de asalto americano', '3', '1', '3', '120'),
('SCAR-L', 'Arma', 'EEUU', 'Rifle de asalto americano', '30', '1', '30', '120'),
('Micro-UZI', 'Arma', 'Alemania', 'Ametralladora ligera', '31', '2', '31', '60'),
('Micro-UZI', 'Arma', 'Alemania', 'Ametralladora ligera', '1', '2', '32', '60'),
('Micro-UZI', 'Arma', 'Alemania', 'Ametralladora ligera', '2', '2', '33', '60'),
('Micro-UZI', 'Arma', 'Alemania', 'Ametralladora ligera', '3', '2', '34', '60'),
('Micro-UZI', 'Arma', 'Alemania', 'Ametralladora ligera', '4', '2', '35', '60'),
('Tommy Gun', 'Arma', 'EEUU', 'Ametralladora clasica', '5', '3', '36', '80'),
('Tommy Gun', 'Arma', 'EEUU', 'Ametralladora clasica', '6', '3', '37', '80'),
('Tommy Gun', 'Arma', 'EEUU', 'Ametralladora clasica', '7', '3', '38', '80'),
('Tommy Gun', 'Arma', 'EEUU', 'Ametralladora clasica', '8', '3', '39', '80'),
('SCAR-L', 'Arma', 'EEUU', 'Rifle de asalto americano', '4', '1', '4', '120'),
('Tommy Gun', 'Arma', 'EEUU', 'Ametralladora clasica', '9', '3', '40', '80'),
('Tommy Gun', 'Arma', 'EEUU', 'Ametralladora clasica', '10', '3', '41', '80'),
('Tommy Gun', 'Arma', 'EEUU', 'Ametralladora clasica', '11', '3', '42', '80'),
('Tommy Gun', 'Arma', 'EEUU', 'Ametralladora clasica', '12', '3', '43', '80'),
('Tommy Gun', 'Arma', 'EEUU', 'Ametralladora clasica', '13', '3', '44', '80'),
('Tommy Gun', 'Arma', 'EEUU', 'Ametralladora clasica', '14', '3', '45', '80'),
('AWM', 'Arma', 'EEUU', 'Rifle francontirador', '15', '4', '46', '200'),
('AWM', 'Arma', 'EEUU', 'Rifle francontirador', '16', '4', '47', '200'),
('AWM', 'Arma', 'EEUU', 'Rifle francontirador', '17', '4', '48', '200'),
('AWM', 'Arma', 'EEUU', 'Rifle francontirador', '18', '4', '49', '200'),
('SCAR-L', 'Arma', 'EEUU', 'Rifle de asalto americano', '5', '1', '5', '120'),
('AWM', 'Arma', 'EEUU', 'Rifle francontirador', '19', '4', '50', '200'),
('AWM', 'Arma', 'EEUU', 'Rifle francontirador', '20', '4', '51', '200'),
('AWM', 'Arma', 'EEUU', 'Rifle francontirador', '21', '4', '52', '200'),
('AWM', 'Arma', 'EEUU', 'Rifle francontirador', '22', '4', '53', '200'),
('AWM', 'Arma', 'EEUU', 'Rifle francontirador', '23', '4', '54', '200'),
('Desert Eagle', 'Arma', 'EEUU', 'Pistola de gran calibre', '24', '5', '55', '55'),
('Desert Eagle', 'Arma', 'EEUU', 'Pistola de gran calibre', '25', '5', '56', '55'),
('Desert Eagle', 'Arma', 'EEUU', 'Pistola de gran calibre', '26', '5', '57', '55'),
('Desert Eagle', 'Arma', 'EEUU', 'Pistola de gran calibre', '27', '5', '58', '55'),
('Desert Eagle', 'Arma', 'EEUU', 'Pistola de gran calibre', '28', '5', '59', '55'),
('SCAR-L', 'Arma', 'EEUU', 'Rifle de asalto americano', '6', '1', '6', '120'),
('Desert Eagle', 'Arma', 'EEUU', 'Pistola de gran calibre', '30', '5', '60', '55'),
('Desert Eagle', 'Arma', 'EEUU', 'Pistola de gran calibre', '31', '5', '61', '55'),
('Granada alhambra', 'Consumible', 'Spain', 'Granada explosiva', '1', '6', '62', '20'),
('Granada alhambra', 'Consumible', 'Spain', 'Granada explosiva', '2', '6', '63', '20'),
('Granada alhambra', 'Consumible', 'Spain', 'Granada explosiva', '3', '6', '64', '20'),
('Granada alhambra', 'Consumible', 'Spain', 'Granada explosiva', '4', '6', '65', '20'),
('Granada alhambra', 'Consumible', 'Spain', 'Granada explosiva', '5', '6', '66', '20'),
('Granada alhambra', 'Consumible', 'Spain', 'Granada explosiva', '6', '6', '67', '20'),
('Granada alhambra', 'Consumible', 'Spain', 'Granada explosiva', '7', '6', '68', '20'),
('Chaleco de proteccion', 'Proteccion', 'China', 'Bien alcolchado', '8', '7', '69', '150'),
('SCAR-L', 'Arma', 'EEUU', 'Rifle de asalto americano', '7', '1', '7', '120'),
('Chaleco de proteccion', 'Proteccion', 'China', 'Bien alcolchado', '9', '7', '70', '150'),
('Chaleco de proteccion', 'Proteccion', 'China', 'Bien alcolchado', '10', '7', '71', '150'),
('Chaleco de proteccion', 'Proteccion', 'China', 'Bien alcolchado', '11', '7', '72', '150'),
('Chaleco de proteccion', 'Proteccion', 'China', 'Bien alcolchado', '12', '7', '73', '150'),
('Chaleco de proteccion', 'Proteccion', 'China', 'Bien alcolchado', '13', '7', '74', '150'),
('Chaleco de proteccion', 'Proteccion', 'China', 'Bien alcolchado', '14', '7', '75', '150'),
('Chaleco de proteccion', 'Proteccion', 'China', 'Bien alcolchado', '15', '7', '76', '150'),
('Gafas balisticas', 'Proteccion', 'China', 'Imprescindibles', '16', '8', '77', '15'),
('Gafas balisticas', 'Proteccion', 'China', 'Imprescindibles', '17', '8', '78', '15'),
('Gafas balisticas', 'Proteccion', 'China', 'Imprescindibles', '18', '8', '79', '15'),
('SCAR-L', 'Arma', 'EEUU', 'Rifle de asalto americano', '8', '1', '8', '120'),
('Gafas balisticas', 'Proteccion', 'China', 'Imprescindibles', '19', '8', '80', '15'),
('Gafas balisticas', 'Proteccion', 'China', 'Imprescindibles', '20', '8', '81', '15'),
('Granadas de humo', 'Consumible', 'Spain', 'Humo blanco', '21', '9', '82', '25'),
('Granadas de humo', 'Consumible', 'Spain', 'Humo blanco', '22', '9', '83', '25'),
('Granadas de humo', 'Consumible', 'Spain', 'Humo blanco', '23', '9', '84', '25'),
('Granadas de humo', 'Consumible', 'Spain', 'Humo blanco', '24', '9', '85', '25'),
('Granadas de humo', 'Consumible', 'Spain', 'Humo blanco', '25', '9', '86', '25'),
('Granadas de humo', 'Consumible', 'Spain', 'Humo blanco', '26', '9', '87', '25'),
('Mira holografica', 'Complemento', 'England', 'Mejora tu punteria', '27', '10', '88', '35'),
('Mira holografica', 'Complemento', 'England', 'Mejora tu punteria', '28', '10', '89', '35'),
('SCAR-L', 'Arma', 'EEUU', 'Rifle de asalto americano', '9', '1', '9', '120'),
('Mira holografica', 'Complemento', 'England', 'Mejora tu punteria', '29', '10', '90', '35'),
('Mira holografica', 'Complemento', 'England', 'Mejora tu punteria', '30', '10', '91', '35'),
('Mira holografica', 'Complemento', 'England', 'Mejora tu punteria', '31', '10', '92', '35');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sales_check`
--

CREATE TABLE IF NOT EXISTS `sales_check` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user` varchar(40) NOT NULL,
  `amount` float NOT NULL,
  `sale_date` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=78 ;

--
-- Volcado de datos para la tabla `sales_check`
--

INSERT INTO `sales_check` (`id`, `user`, `amount`, `sale_date`) VALUES
(52, 'Yester', 480, '2018-03-01'),
(55, 'Yester', 0, '2018-03-01'),
(56, 'Yester', 120, '2018-03-01'),
(57, 'Yester', 0, '2018-03-01'),
(58, 'Yester', 0, '2018-03-01'),
(59, 'Yester', 0, '2018-03-01'),
(60, 'Yester', 0, '2018-03-01'),
(61, 'Yester', 0, '2018-03-01'),
(62, 'Yester', 0, '2018-03-01'),
(63, 'Yester', 0, '2018-03-01'),
(64, 'Yester', 0, '2018-03-01'),
(65, 'Yester', 0, '2018-03-01'),
(66, 'Yester', 120, '2018-03-01'),
(67, 'Yester', 0, '2018-03-01'),
(68, 'Yester', 0, '2018-03-01'),
(69, 'Yester', 120, '2018-03-01'),
(75, 'Yester', 540, '2018-03-05'),
(76, 'Yester', 360, '2018-03-06'),
(77, 'Yester', 240, '2018-03-07');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `shop`
--

CREATE TABLE IF NOT EXISTS `shop` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `cityID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `shop`
--

INSERT INTO `shop` (`id`, `name`, `cityID`) VALUES
(1, 'AirSanJose', 1),
(2, 'SoftSanRaja', 1),
(3, 'El llombo Airsoft', 1),
(4, 'Santo Airsoft Domingo', 1),
(5, 'El tiro al plato', 1),
(6, 'GunnerAirsoft', 2),
(7, 'Replicas del mundo', 2),
(8, 'La garita militar', 2),
(9, 'Airsoft Recon', 2),
(10, 'Todo proteccion', 2),
(11, 'Coronel Airsoft', 3),
(12, 'Soldiers', 3),
(13, 'Avalorios Airsoft', 3),
(14, 'Airsoft Hobby', 3),
(15, 'Airsoft Levante', 3),
(16, 'Global Airsoft', 4),
(17, 'PlayerUnknow Airsoft', 4),
(18, 'Call of Airsoft', 4),
(19, 'Medal of Airsoft', 4),
(20, 'Ghost of Airsoft', 4),
(21, 'Desenfunda', 5),
(22, 'Duelista', 5),
(23, 'BattleGround', 5),
(24, 'Clash Airsoft', 5),
(25, 'Armaggedon', 5),
(26, 'Socarrats armats', 6),
(27, 'Mooooza, que tengo armas', 7),
(28, 'Ragnarok', 7),
(29, 'Buenos Airsoft', 7),
(30, 'Softonic', 8),
(31, 'Softisticado', 8);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_name` varchar(30) NOT NULL,
  `password` varchar(40) NOT NULL,
  `country` varchar(60) DEFAULT NULL,
  `birthday` varchar(30) DEFAULT NULL,
  `email` varchar(255) NOT NULL DEFAULT '',
  `type` int(2) NOT NULL,
  PRIMARY KEY (`email`),
  UNIQUE KEY `user_name` (`user_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`user_name`, `password`, `country`, `birthday`, `email`, `type`) VALUES
('Avato92', 'yester', 'Spain', '27/09/1992', 'avato92@gmail.com', 2),
('Yester', 'yester', NULL, NULL, 'yesterquiff@gmail.com', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `weapons`
--

CREATE TABLE IF NOT EXISTS `weapons` (
  `weapon_name` varchar(30) NOT NULL,
  `caliber` varchar(40) NOT NULL,
  `country` varchar(60) NOT NULL,
  `datepicker` varchar(30) NOT NULL,
  `complements` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`weapon_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `weapons`
--

INSERT INTO `weapons` (`weapon_name`, `caliber`, `country`, `datepicker`, `complements`, `description`) VALUES
('HK-G36', '5,56mm x 45mm', 'Alemania', '12/12/1936', 'Mira telescopica', 'Arma rusa capaz de matar a 800 metros de distancia'),
('M-16', '5,56mm x 45mm', 'EEUU', '12/12/1936', 'Mira telescopica', 'Arma rusa capaz de matar a 800 metros de distancia');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
