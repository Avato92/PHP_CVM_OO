<?php
    if ((isset($_GET['page'])) && ($_GET['page']==="controller_weapon")){
		include("view/inc/top_page_weapon.php");
	}elseif((isset($_GET['page'])) && ($_GET['page']==="controller_shop")){
        include("view/inc/top_page_shop.php");
    }elseif((isset($_GET['page'])) && ($_GET['page']==="controller_login")){
        include("view/inc/top_page_login.php");
    }elseif((isset($_GET['page'])) && ($_GET['page']==="controller_weather")){
        include("view/inc/top_page_weather.php");
    }elseif((isset($_GET['page']))&& ($_GET['page']==="controller_profile")){
        include("view/inc/top_page_profile.php");
    }else{
		include("view/inc/top_page.php");
	}
	session_start();

    if (isset($_POST["lang"])){
        $lang = $_POST["lang"];
        $_SESSION ["lang"] = $lang;
    }

    if(isset($_SESSION['lang'])){
        include ('lang/lang.php');
    }else{
        include ('lang/es.php');
    }
    

?>
<div id="wrapper">		
    <div id="menu">
		<?php
        if(isset($_SESSION['user'])){
            if($_SESSION['type']==1){
                include("view/inc/menu.php"); 
            }else{
                include("view/inc/menu_admin.php");
            }
		   
        }else{
            include("view/inc/menu_no_auth.php");
        }
		?>
    </div>	
    <div id="">
    	<?php 
		    include("view/inc/pages.php"); 
		?>        
        <br style="clear:both;" />
    </div>
    <div id="footer">   	   
	    <?php
	        include("view/inc/footer.php");
	    ?>        
    </div>
</div>
<?php
    include("view/inc/bottom_page.php");
