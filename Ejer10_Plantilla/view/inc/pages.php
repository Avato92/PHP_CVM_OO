<?php
	if(isset($_GET['page'])){
	switch($_GET['page']){
		case "homepage";
			include("module/dummies/controller/controller_dummies.php");
			break;
		case "controller_weapon";
			include("module/weapons/controller/".$_GET['page'].".php");
			break;
		case "services";
			include("module/services/controller/controller_services.php");
			break;
		case "aboutus";
			include("module/aboutus/controller/controller_about_us.php");
			break;
		case "contactus";
			include("module/contact/controller/controller_contact.php");
			break;
		case "controller_shop";
			include("module/shop/controller/".$_GET['page'].".php");
			break;
		case "controller_login";
			include("module/login/controller/".$_GET['page'].".php");
			break;
		case "controller_weather";
			include("module/weather/controller/".$_GET['page'].".php");
			break;
		case "controller_profile";
			include("module/profile/controller/".$_GET['page'].".php");
			break;
		case "404";
			include("view/inc/error".$_GET['page'].".php");
			break;
		case "503";
			include("view/inc/error".$_GET['page'].".php");
			break;
		case "504";
			include("view/inc/error".$_GET['page'].".php");
			break;
		default;
			include("module/dummies/controller/controller_dummies.php");
			break;
		}
	}else{
		include ("module/dummies/controller/controller_dummies.php");
	}
