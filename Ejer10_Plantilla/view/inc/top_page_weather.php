<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
          <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
              <!--JS-->
          <script src="module/weather/model/weather.js"></script>
          <!-- CSS -->
          <link href="module/weather/view/weather.css" rel="stylesheet" type="text/css" />
         <!--Template-->
  	 	   <link href="view/css/bootstrap.css" rel="stylesheet" />
    <!-- FONTAWESOME STYLE CSS -->
    	   <link href="view/css/font-awesome.min.css" rel="stylesheet" />
    <!-- CUSTOM STYLE CSS -->
    	   <link href="view/css/style.css" rel="stylesheet" /> 
         <!-- MODAL -->
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="/resources/demos/style.css">
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> 
  	</head>
  	<body>